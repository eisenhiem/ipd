/*
Navicat MySQL Data Transfer

Source Server         : Local_DB
Source Server Version : 50565
Source Host           : localhost:3306
Source Database       : hi

Target Server Type    : MYSQL
Target Server Version : 50565
File Encoding         : 65001

Date: 2020-01-21 20:07:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- View structure for `cbc`
-- ----------------------------
DROP VIEW IF EXISTS `cbc`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cbc` AS select `o`.`an` AS `an`,`l`.`hn` AS `hn`,`l`.`srvdate` AS `srvdate`,cast((`l`.`srvtime` * 100) as time) AS `srv_time`,`c`.`hb` AS `hb`,`c`.`hct` AS `hct`,`c`.`wbc` AS `wbc`,`c`.`pltc` AS `pltc`,`c`.`pmn` AS `pmn`,`c`.`lym` AS `lym` from ((`lbbk` `l` join `ovst` `o` on((`l`.`vn` = `o`.`vn`))) join `lcbc` `c` on(((`l`.`ln` = `c`.`ln`) and (`l`.`finish` = 1)))) where ((cast(`l`.`vstdttm` as date) between cast((now() - interval 7 day) as date) and cast(now() as date)) and (`o`.`an` <> 0)) group by `o`.`an` order by `l`.`srvdate` ;

-- ----------------------------
-- View structure for `charge`
-- ----------------------------
DROP VIEW IF EXISTS `charge`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `charge` AS select `i`.`hn` AS `hn`,`i`.`an` AS `an`,`p`.`fname` AS `fname`,`p`.`lname` AS `lname`,`i`.`dchdate` AS `dchdate`,concat('http://hiserver5/charge/',`i`.`an`,'.pdf') AS `link` from (`ipt` `i` join `pt` `p` on((`i`.`hn` = `p`.`hn`))) where (`i`.`an` > 62001000) order by `i`.`hn` ;

-- ----------------------------
-- View structure for `dc`
-- ----------------------------
DROP VIEW IF EXISTS `dc`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `dc` AS select `i`.`an` AS `an`,`i`.`hn` AS `hn`,`p`.`pop_id` AS `cid`,if((`p`.`pname` = ''),cast((case `p`.`male` when 1 then if((`p`.`mrtlst` < 6),if((timestampdiff(YEAR,`p`.`brthdate`,now()) < 15),'ด.ช.','นาย'),if((timestampdiff(YEAR,`p`.`brthdate`,now()) < 20),'เณร','พระ')) when 2 then if((`p`.`mrtlst` = 1),if((timestampdiff(YEAR,`p`.`brthdate`,now()) < 15),'ด.ญ.','น.ส.'),if((`p`.`mrtlst` < 6),'นาง','แม่ชี')) end) as char(8) charset utf8),convert(`p`.`pname` using utf8)) AS `prename`,concat(`p`.`fname`,' ',`p`.`lname`) AS `ptname`,timestampdiff(YEAR,`p`.`brthdate`,`i`.`rgtdate`) AS `age`,`p`.`brthdate` AS `brthdate`,`x`.`namemale` AS `sex`,`n`.`namentnlty` AS `nation`,`g`.`namerlgn` AS `religion`,`i`.`bw` AS `bw`,`t`.`namepttype` AS `pt_type`,concat(`p`.`addrpart`,' ',`m`.`namemoob`,' ',`b`.`nametumb`,' ',`a`.`nameampur`,' ',`c`.`namechw`) AS `address`,`p`.`hometel` AS `hometel`,`s`.`namemrt` AS `married_status`,`j`.`nameoccptn` AS `job`,`p`.`infmname` AS `relate_person`,`p`.`statusinfo` AS `relation`,`p`.`infmaddr` AS `relate_addr`,`p`.`infmtel` AS `infmtel`,`hi`.`idpm`.`nameidpm` AS `ward`,substr(`w`.`bedno`,3,8) AS `bed`,`i`.`rgtdate` AS `rgtdate`,cast((`i`.`rgttime` * 100) as time) AS `rg_time`,`i`.`dchdate` AS `dchdate`,cast((`i`.`dchtime` * 100) as time) AS `dc_time`,if((timestampdiff(HOUR,concat(`i`.`rgtdate`,' ',cast((`i`.`rgttime` * 100) as time)),concat(`i`.`dchdate`,' ',cast((`i`.`dchtime` * 100) as time))) between 4 and 24),1,(timestampdiff(DAY,`i`.`rgtdate`,`i`.`dchdate`) + if(((timestampdiff(HOUR,concat(`i`.`rgtdate`,' ',cast((`i`.`rgttime` * 100) as time)),concat(`i`.`dchdate`,' ',cast((`i`.`dchtime` * 100) as time))) % 24) > 6),1,0))) AS `losd`,`h`.`hosname` AS `refer_hosp`,`cs`.`namerfrcs` AS `refer_cause`,group_concat(distinct `f`.`fudate` separator ',') AS `follow_up`,concat(`lp`.`prename`,`hi`.`dct`.`fname`,' ',`hi`.`dct`.`lname`) AS `doctor`,`hi`.`dct`.`lcno` AS `lcno` from ((((((((((((((((((((`ipt` `i` join `pt` `p` on((`i`.`hn` = `p`.`hn`))) join `pttype` `t` on((`i`.`pttype` = `t`.`pttype`))) left join `changwat` `c` on((`p`.`chwpart` = `c`.`chwpart`))) left join `ampur` `a` on(((`p`.`chwpart` = `a`.`chwpart`) and (`p`.`amppart` = `a`.`amppart`)))) left join `tumbon` `b` on(((`p`.`chwpart` = `b`.`chwpart`) and (`p`.`amppart` = `b`.`amppart`) and (`p`.`tmbpart` = `b`.`tmbpart`)))) left join `mooban` `m` on(((`p`.`chwpart` = `m`.`chwpart`) and (`p`.`amppart` = `m`.`amppart`) and (`p`.`tmbpart` = `m`.`tmbpart`) and (`p`.`moopart` = `m`.`moopart`)))) left join `mrtlst` `s` on((`p`.`mrtlst` = `s`.`mrtlst`))) left join `male` `x` on((`p`.`male` = `x`.`male`))) left join `iptadm` `w` on((`i`.`an` = `w`.`an`))) left join `idpm` on((substr(`w`.`bedno`,1,1) = `hi`.`idpm`.`idpm`))) left join `occptn` `j` on((`p`.`occptn` = `j`.`occptn`))) left join `ntnlty` `n` on((`p`.`ntnlty` = `n`.`ntnlty`))) left join `rlgn` `g` on((`p`.`rlgn` = `g`.`rlgn`))) left join `orfro` `r` on((`i`.`an` = `r`.`an`))) left join `rfrcs` `cs` on((`r`.`rfrcs` = `cs`.`rfrcs`))) left join `chospital` `h` on((convert(`r`.`rfrlct` using utf8) = `h`.`hoscode`))) left join `oapp` `f` on((`i`.`an` = `f`.`an`))) left join `iptdx` `ix` on(((`i`.`an` = `ix`.`an`) and (`ix`.`itemno` = 1)))) left join `dct` on((`ix`.`dct` = `hi`.`dct`.`lcno`))) left join `l_prename` `lp` on((`hi`.`dct`.`pname` = `lp`.`prename_code`))) where (`i`.`dchdate` between cast((now() - interval 1 week) as date) and cast(now() as date)) group by `i`.`an` ;

-- ----------------------------
-- View structure for `elyte`
-- ----------------------------
DROP VIEW IF EXISTS `elyte`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `elyte` AS select `o`.`an` AS `an`,`l`.`hn` AS `hn`,`l`.`srvdate` AS `srvdate`,cast((`l`.`srvtime` * 100) as time) AS `srv_time`,`c`.`k` AS `k`,`c`.`na` AS `na`,`c`.`cl` AS `cl`,`c`.`co2` AS `co2` from ((`lbbk` `l` join `ovst` `o` on((`l`.`vn` = `o`.`vn`))) join `lelyte` `c` on(((`l`.`ln` = `c`.`ln`) and (`l`.`finish` = 1)))) where ((cast(`l`.`vstdttm` as date) between cast((now() - interval 7 day) as date) and cast(now() as date)) and (`o`.`an` <> 0)) group by `o`.`an` order by `l`.`srvdate` ;

-- ----------------------------
-- View structure for `l_cr`
-- ----------------------------
DROP VIEW IF EXISTS `l_cr`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `l_cr` AS select `l`.`hn` AS `hn`,max(`l`.`vstdttm`) AS `last_cr`,max(`l`.`ln`) AS `last_ln`,max(timestampdiff(YEAR,`p`.`brthdate`,`l`.`vstdttm`)) AS `age`,`p`.`male` AS `sex` from ((`lbbk` `l` join `lbuncr` `cr` on(((`l`.`ln` = `cr`.`ln`) and (`cr`.`creat` <> '') and (`l`.`finish` = 1)))) join `pt` `p` on((`l`.`hn` = `p`.`hn`))) group by `l`.`hn` ;

-- ----------------------------
-- View structure for `list_ipd`
-- ----------------------------
DROP VIEW IF EXISTS `list_ipd`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `list_prsc` AS select `i`.`an` AS `an`,`m`.`name` AS `dname`,`d`.`qty` AS `qty`,`d`.`medusage` AS `medusage`,`d`.`xdoseno` AS `xdoseno`,`m`.`pres_unt` AS `unit` from (((`ipt` `i` join `prsc` `p` on(((`i`.`an` = `p`.`an`) and (`i`.`dchdate` = `p`.`prscdate`) and (`i`.`dchtime` = `p`.`prsctime`)))) join `prscdt` `d` on((`p`.`prscno` = `d`.`prscno`))) join `meditem` `m` on((`d`.`meditem` = `m`.`meditem`))) where (`i`.`dchdate` between cast((now() - interval 5 month) as date) and cast(now() as date));
-- ----------------------------
-- View structure for `list_lab`
-- ----------------------------
DROP VIEW IF EXISTS `list_lab`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `list_lab` AS select `o`.`an` AS `an`,min((case `l`.`labcode` when '001' then `l`.`ln` end)) AS `fcbc`,max((case `l`.`labcode` when '001' then `l`.`ln` end)) AS `lcbc`,min((case `l`.`labcode` when '031' then `l`.`ln` end)) AS `felyte`,max((case `l`.`labcode` when '031' then `l`.`ln` end)) AS `lelyte`,min((case `l`.`labcode` when '222' then `l`.`ln` end)) AS `fcreat`,max((case `l`.`labcode` when '222' then `l`.`ln` end)) AS `lcreat`,min((case `l`.`labcode` when '001' then date_format(concat(if((`l`.`srvdate` = '0000-00-00'),`l`.`senddate`,`l`.`srvdate`),' ',cast((if((`l`.`srvtime` = 0),`l`.`sendtime`,`l`.`srvtime`) * 100) as time)),'%Y-%m-%d %H:%i') end)) AS `fdate_cbc`,max((case `l`.`labcode` when '001' then date_format(concat(if((`l`.`srvdate` = '0000-00-00'),`l`.`senddate`,`l`.`srvdate`),' ',cast((if((`l`.`srvtime` = 0),`l`.`sendtime`,`l`.`srvtime`) * 100) as time)),'%Y-%m-%d %H:%i') end)) AS `ldate_cbc`,min((case `l`.`labcode` when '031' then date_format(concat(if((`l`.`srvdate` = '0000-00-00'),`l`.`senddate`,`l`.`srvdate`),' ',cast((if((`l`.`srvtime` = 0),`l`.`sendtime`,`l`.`srvtime`) * 100) as time)),'%Y-%m-%d %H:%i') end)) AS `fdate_elyte`,max((case `l`.`labcode` when '031' then date_format(concat(if((`l`.`srvdate` = '0000-00-00'),`l`.`senddate`,`l`.`srvdate`),' ',cast((if((`l`.`srvtime` = 0),`l`.`sendtime`,`l`.`srvtime`) * 100) as time)),'%Y-%m-%d %H:%i') end)) AS `ldate_elyte`,min((case `l`.`labcode` when '222' then date_format(concat(if((`l`.`srvdate` = '0000-00-00'),`l`.`senddate`,`l`.`srvdate`),' ',cast((if((`l`.`srvtime` = 0),`l`.`sendtime`,`l`.`srvtime`) * 100) as time)),'%Y-%m-%d %H:%i') end)) AS `fdate_creat`,max((case `l`.`labcode` when '222' then date_format(concat(if((`l`.`srvdate` = '0000-00-00'),`l`.`senddate`,`l`.`srvdate`),' ',cast((if((`l`.`srvtime` = 0),`l`.`sendtime`,`l`.`srvtime`) * 100) as time)),'%Y-%m-%d %H:%i') end)) AS `ldate_creat` from (`ovst` `o` join `lbbk` `l` on(((`o`.`vn` = `l`.`vn`) and (`l`.`labcode` in ('001','029','031','222')) and (`l`.`finish` = 1)))) where ((`o`.`an` <> 0) and (cast(`l`.`vstdttm` as date) between cast((now() - interval 30 day) as date) and cast(now() as date)) and (`o`.`an` <> 0)) group by `o`.`an` ;

-- ----------------------------
-- View structure for `list_prsc`
-- ----------------------------
DROP VIEW IF EXISTS `list_prsc`;
CREATE ALGORITHM=UNDEFINED DEFINER=`hiuser`@`%` SQL SECURITY DEFINER VIEW `list_prsc` AS select `i`.`an` AS `an`,`m`.`name` AS `dname`,`d`.`qty` AS `qty`,`m`.`pres_unt` AS `unit` from (((`ipt` `i` join `prsc` `p` on(((`i`.`an` = `p`.`an`) and (`i`.`dchdate` = `p`.`prscdate`) and (`i`.`dchtime` = `p`.`prsctime`)))) join `prscdt` `d` on((`p`.`prscno` = `d`.`prscno`))) join `meditem` `m` on((`d`.`meditem` = `m`.`meditem`))) where (`i`.`dchdate` between cast((now() - interval 7 day) as date) and cast(now() as date)) ;

-- ----------------------------
-- View structure for `view_dtx`
-- ----------------------------
DROP VIEW IF EXISTS `view_dtx`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_dtx` AS select `i`.`an` AS `an`,concat(`l`.`srvdate`,' ',cast((`l`.`srvtime` * 100) as time)) AS `servdate`,replace(substr(`x`.`dtx`,1,3),'*','') AS `result` from ((`lbbk` `l` join `ipt` `i` on((`l`.`vn` = `i`.`vn`))) join `ldtx` `x` on(((`l`.`ln` = `x`.`ln`) and (`l`.`finish` = 1)))) where ((replace(substr(`x`.`dtx`,1,3),'*','') between '0' and '999') and (`i`.`dchdate` between (now() - interval 30 day) and now())) ;
