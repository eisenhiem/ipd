<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Charge;

/**
 * ChargeSearch represents the model behind the search form of `app\models\Charge`.
 */
class ChargeSearch extends Charge
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hn', 'an'], 'integer'],
            [['fname', 'lname', 'dchdate', 'link'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Charge::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'hn' => $this->hn,
            'an' => $this->an,
            'dchdate' => $this->dchdate,
        ]);

        $query->andFilterWhere(['like', 'fname', $this->fname])
            ->andFilterWhere(['like', 'lname', $this->lname])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
