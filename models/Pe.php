<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pe".
 *
 * @property int $vn
 * @property string $pe
 */
class Pe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn'], 'required'],
            [['vn'], 'integer'],
            [['pe'], 'string'],
        ];
    }

    public static function primaryKey(){
        return ['vn'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'pe' => 'Pe',
        ];
    }
}
