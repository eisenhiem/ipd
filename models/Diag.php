<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "diag".
 *
 * @property int $an
 * @property string $pdx_code
 * @property string $pdx_name
 * @property string $comb_code
 * @property string $comb_name
 * @property string $comp_code
 * @property string $comp_name
 * @property string $other_code
 * @property string $other_name
 * @property string $ex_code
 * @property string $ex_name
 */
class Diag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'diag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an'], 'integer'],
            [['comb_name', 'comp_name', 'other_name', 'ex_name'], 'string'],
            [['pdx_code'], 'string', 'max' => 7],
            [['pdx_name'], 'string', 'max' => 90],
            [['comb_code', 'comp_code', 'other_code', 'ex_code'], 'string', 'max' => 50],
        ];
    }

    public static function primaryKey(){
        return ['an'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'pdx_code' => 'Pdx Code',
            'pdx_name' => 'Pdx Name',
            'comb_code' => 'Comb Code',
            'comb_name' => 'Comb Name',
            'comp_code' => 'Comp Code',
            'comp_name' => 'Comp Name',
            'other_code' => 'Other Code',
            'other_name' => 'Other Name',
            'ex_code' => 'Ex Code',
            'ex_name' => 'Ex Name',
        ];
    }
}
