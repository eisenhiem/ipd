<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dc;

/**
 * DcSearch represents the model behind the search form of `app\models\Dc`.
 */
class DcSearch extends Dc
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'hn', 'age', 'losd'], 'integer'],
            [['prename','cid', 'ptname','brthdate' , 'sex', 'nation', 'religion', 'pt_type', 'address', 'hometel', 'married_status', 'job', 'relate_person', 'relation', 'relate_addr','infmtel', 'bed', 'rgtdate', 'rg_time', 'dchdate', 'dc_time', 'refer_hosp','refer_cause' ,'follow_up','dc_type','dc_status'], 'safe'],
            [['bw'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'an' => $this->an,
            'hn' => $this->hn,
            'age' => $this->age,
            'bw' => $this->bw,
            'rgtdate' => $this->rgtdate,
            'rg_time' => $this->rg_time,
            'dchdate' => $this->dchdate,
            'dc_time' => $this->dc_time,
            'losd' => $this->losd,
        ]);

        $query->andFilterWhere(['like', 'prename', $this->prename])
            ->andFilterWhere(['like', 'ptname', $this->ptname])
            ->andFilterWhere(['like', 'cid', $this->cid])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'nation', $this->nation])
            ->andFilterWhere(['like', 'religion', $this->religion])
            ->andFilterWhere(['like', 'pt_type', $this->pt_type])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'hometel', $this->hometel])
            ->andFilterWhere(['like', 'married_status', $this->married_status])
            ->andFilterWhere(['like', 'job', $this->job])
            ->andFilterWhere(['like', 'brthdate', $this->brthdate])
            ->andFilterWhere(['like', 'relate_person', $this->relate_person])
            ->andFilterWhere(['like', 'relation', $this->relation])
            ->andFilterWhere(['like', 'relate_addr', $this->relate_addr])
            ->andFilterWhere(['like', 'infmtel', $this->infmtel])            
            ->andFilterWhere(['like', 'ward', $this->ward])
            ->andFilterWhere(['like', 'bed', $this->bed])
            ->andFilterWhere(['like', 'refer_hosp', $this->refer_hosp])
            ->andFilterWhere(['like', 'follow_up', $this->follow_up]);

        return $dataProvider;
    }
}
