<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lbbk".
 *
 * @property int $ln
 * @property string $lfudate
 * @property string $labcode
 * @property int $vn
 * @property string $reportby
 * @property string $requestby
 * @property int $fufinish
 * @property string $vstdttm
 * @property int $hn
 * @property int $an
 * @property string $senddate
 * @property int $sendtime
 * @property string $srvdate
 * @property int $srvtime
 * @property string $pttype
 * @property int $finish
 * @property string $c2automate
 * @property string $hcode
 * @property string $approve
 * @property string $approvedate
 * @property string $approvetime
 * @property string $lcomment
 * @property string $labcomment Comment of lab
 * @property string $labgroup
 * @property string $pdffile
 * @property string $accept
 * @property string $acceptby
 * @property string $accepttime
 */
class Lbbk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lbbk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lfudate', 'vstdttm', 'senddate', 'srvdate', 'approvedate'], 'safe'],
            [['labcode', 'vn', 'hn', 'an', 'sendtime', 'srvtime', 'pttype', 'c2automate', 'hcode', 'lcomment', 'labcomment', 'labgroup', 'acceptby', 'accepttime'], 'required'],
            [['vn', 'fufinish', 'hn', 'an', 'sendtime', 'srvtime', 'finish'], 'integer'],
            [['labcode'], 'string', 'max' => 3],
            [['reportby', 'requestby', 'approve'], 'string', 'max' => 100],
            [['pttype', 'labgroup'], 'string', 'max' => 2],
            [['c2automate', 'lcomment', 'accept'], 'string', 'max' => 1],
            [['hcode'], 'string', 'max' => 5],
            [['approvetime', 'accepttime'], 'string', 'max' => 4],
            [['labcomment'], 'string', 'max' => 255],
            [['pdffile', 'acceptby'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ln' => 'Ln',
            'lfudate' => 'Lfudate',
            'labcode' => 'Labcode',
            'vn' => 'Vn',
            'reportby' => 'Reportby',
            'requestby' => 'Requestby',
            'fufinish' => 'Fufinish',
            'vstdttm' => 'Vstdttm',
            'hn' => 'Hn',
            'an' => 'An',
            'senddate' => 'Senddate',
            'sendtime' => 'Sendtime',
            'srvdate' => 'Srvdate',
            'srvtime' => 'Srvtime',
            'pttype' => 'Pttype',
            'finish' => 'Finish',
            'c2automate' => 'C2automate',
            'hcode' => 'Hcode',
            'approve' => 'Approve',
            'approvedate' => 'Approvedate',
            'approvetime' => 'Approvetime',
            'lcomment' => 'Lcomment',
            'labcomment' => 'Labcomment',
            'labgroup' => 'Labgroup',
            'pdffile' => 'Pdffile',
            'accept' => 'Accept',
            'acceptby' => 'Acceptby',
            'accepttime' => 'Accepttime',
        ];
    }
}
