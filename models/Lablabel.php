<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lab".
 *
 * @property string $labcode
 * @property string $labname
 * @property int $pricelab
 * @property int $pricelabcgd
 * @property string $ptright
 * @property string $dbf
 * @property string $dbfs
 * @property string $cgd
 * @property string $formi
 * @property string $forme
 * @property string $formh
 * @property string $laballow
 * @property string $etype
 * @property string $formp
 * @property string $unit
 */
class Lablabel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lablabel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['labcode', 'labname','id'],'required'],
            [['fieldname','fieldlabel'],'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'labcode' => 'Labcode',
            'labname' => 'Labname',
            'fieldname' => 'Field Name',
            'fieldlabel' => 'Label',
        ];
    }
}
