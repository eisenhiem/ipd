<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ListIpd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'list_ipd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an', 'hn', 'age', 'sbp', 'dbp', 'pr', 'rr','lcno'], 'integer'],
            [['ptname','cid' , 'cc', 'pi', 'ph', 'pe', 'prediag', 'doctor','prename'], 'safe'],
            [['bw', 'tt', 'height', 'bmi'], 'number'],
        ];
    }

    public static function primaryKey(){
        return ['an'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'AN',
            'hn' => 'HN',
            'cid' =>'CID',
            'age' => 'Age',
            'sbp' => 'SBP',
            'dbp' => 'DBP',
            'pr' => 'Pluse Rate',
            'rr' => 'Resporatory Rate',
            'prename' => 'Pname',
            'ptname' => 'Patient Name',
            'doctor' => 'Attending Physicial',
            'cc' => 'CC',
            'pi' => 'Present Illness',
            'ph' => 'Past History',
            'pe' => 'Physical Exam',
            'prediag' => 'Pre Diagnosis',
            'bw' => 'Body Weight',
            'tt' => 'Body Temp',
            'height' => 'Height',
            'bmi' => 'BMI',
            'lcno' => 'License No',
            'prename' => 'Pname',
        ];
    }

}
