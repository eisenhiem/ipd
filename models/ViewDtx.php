<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ViewDtx extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'view_dtx';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['result'], 'integer'],
            [['servdate'], 'safe'],            
        ];
    }

    public static function primaryKey(){
        return ['an','servdate'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'AN',
            'result' => 'DTX',
            'servdate' => 'Report Date',
        ];
    }

}
