<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visitnoteconsult".
 *
 * @property int $id
 * @property int $vn
 * @property string $detail
 * @property string $srvdttm
 * @property string $dct
 */
class Visitnoteconsult extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visitnoteconsult';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'detail', 'srvdttm', 'dct'], 'required'],
            [['vn'], 'integer'],
            [['srvdttm'], 'safe'],
            [['detail'], 'string', 'max' => 500],
            [['dct'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vn' => 'Vn',
            'detail' => 'Detail',
            'srvdttm' => 'Srvdttm',
            'dct' => 'Dct',
        ];
    }
}
