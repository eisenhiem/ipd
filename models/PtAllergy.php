<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pt_allergy".
 *
 * @property int $hn
 * @property string $drug
 */
class PtAllergy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pt_allergy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hn'], 'required'],
            [['hn'], 'integer'],
            [['drug'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hn' => 'Hn',
            'drug' => 'Drug',
        ];
    }
}
