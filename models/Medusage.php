<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "medusage".
 *
 * @property int $id
 * @property string $dosecode
 * @property string $dosename
 * @property string $doseprn1
 * @property string $doseprn2
 * @property string $medclass
 */
class Medusage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'medusage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dosecode', 'dosename', 'doseprn1', 'doseprn2', 'medclass'], 'required'],
            [['dosecode', 'dosename'], 'string', 'max' => 10],
            [['doseprn1', 'doseprn2'], 'string', 'max' => 50],
            [['medclass'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dosecode' => 'Dosecode',
            'dosename' => 'Dosename',
            'doseprn1' => 'Doseprn1',
            'doseprn2' => 'Doseprn2',
            'medclass' => 'Medclass',
        ];
    }
}
