<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ListPrsc;

/**
 * ListPrscSearch represents the model behind the search form of `app\models\ListPrsc`.
 */
class ListPrscSearch extends ListPrsc
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'qty'], 'integer'],
            [['dname', 'unit','medusage','xdoseno'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ListPrsc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'an' => $this->an,
            'qty' => $this->qty,
        ]);

        $query->andFilterWhere(['like', 'dname', $this->dname])
        ->andFilterWhere(['like', 'unit', $this->unit]);

        return $dataProvider;
    }
}
