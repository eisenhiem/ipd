<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nurse_note".
 *
 * @property int $an
 * @property string $note_date
 * @property string $temp
 * @property string $pluse
 * @property string $rr
 * @property string $bp
 * @property string $prob
 * @property string $imp
 * @property string $eva
 * @property string $sig
 */
class NurseNote extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nurse_note';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'integer'],
            [['note_date', 'temp', 'pluse', 'rr', 'bp', 'prob', 'imp', 'eva', 'sig'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'note_date' => 'Note Date',
            'temp' => 'Temp',
            'pluse' => 'Pluse',
            'rr' => 'Rr',
            'bp' => 'Bp',
            'prob' => 'Prob',
            'imp' => 'Imp',
            'eva' => 'Eva',
            'sig' => 'Sig',
        ];
    }
}
