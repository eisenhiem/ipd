<?php

namespace app\models;

use Yii;
use yii\base\Model;

class LCr extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'l_cr';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hn'], 'required'],
            [['last_ln','age','sex','hn'], 'integer'],
            [['last_cr'], 'safe'],            
        ];
    }

    public static function primaryKey(){
        return ['hn'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hn' => 'HN',
            'last_ln' => 'Lab No',
            'age' => 'Age',
            'sex' => 'Sex',
            'last_cr' => 'Date Creatinine',
        ];
    }

}
