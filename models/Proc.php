<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Proc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an', 'O2', 'EKG','ID','Dressing','Blood','CPR','DEBRIDEMENT','US','TUBE'], 'integer'],
            [['proccode'], 'safe'],
        ];
    }

    public static function primaryKey(){
        return ['an'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'AN',
            'O2' => 'HN',
            'EKG' => 'EKG',
            'ID' => 'I & D',
            'Dressing' => 'Dressing',
            'Blood' => 'Transfusion of packed cell',
            'CPR' => 'CPR',
            'DEBRIDEMENT' => 'Debridement',
            'TUBE' => 'ET TUBE',
            'US' => 'Ultrasound',
            'proccode' => 'Procedure Code',
        ];
    }
}
