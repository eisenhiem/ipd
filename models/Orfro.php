<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orfro".
 *
 * @property int $vn
 * @property int $rfrno
 * @property int $an
 * @property string $ward
 * @property string $rfrlct
 * @property string $icd10
 * @property string $rfrcs
 * @property string $cln
 * @property string $vstdate
 * @property int $vsttime
 * @property string $dct
 * @property string $rfrtype
 * @property string $km
 * @property int $pricerefer
 * @property int $loads
 */
class Orfro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orfro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'rfrno', 'an', 'ward', 'rfrlct', 'icd10', 'rfrcs', 'vstdate', 'vsttime', 'dct', 'rfrtype', 'km', 'pricerefer'], 'required'],
            [['vn', 'rfrno', 'an', 'vsttime', 'pricerefer', 'loads'], 'integer'],
            [['vstdate'], 'safe'],
            [['ward', 'rfrtype'], 'string', 'max' => 2],
            [['rfrlct'], 'string', 'max' => 6],
            [['icd10'], 'string', 'max' => 7],
            [['rfrcs'], 'string', 'max' => 1],
            [['cln', 'dct', 'km'], 'string', 'max' => 5],
            [['rfrno'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'rfrno' => 'Rfrno',
            'an' => 'An',
            'ward' => 'Ward',
            'rfrlct' => 'Rfrlct',
            'icd10' => 'Icd10',
            'rfrcs' => 'Rfrcs',
            'cln' => 'Cln',
            'vstdate' => 'Vstdate',
            'vsttime' => 'Vsttime',
            'dct' => 'Dct',
            'rfrtype' => 'Rfrtype',
            'km' => 'Km',
            'pricerefer' => 'Pricerefer',
            'loads' => 'Loads',
        ];
    }
}
