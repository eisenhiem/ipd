<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clinic".
 *
 * @property string $cln
 * @property string $namecln
 */
class Clinic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clinic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namecln'], 'required'],
            [['cln'], 'string', 'max' => 5],
            [['namecln'], 'string', 'max' => 25],
        ];
    }

    public static function primaryKey(){
        return ['cln'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cln' => 'Cln',
            'namecln' => 'Namecln',
        ];
    }
}
