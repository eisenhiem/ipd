<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ptinfo".
 *
 * @property int $hn
 * @property int $age
 * @property string $prename
 * @property string $ptname
 * @property string $cid
 * @property string $birth
 * @property string $sex
 * @property string $address
 */
class Viewfood extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'viewfood';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hn','an','age'], 'integer'],
            [['calendar_date'], 'safe'],
            [['bed'], 'string', 'max' => 5],
        ];
    }

    public static function primaryKey(){
        return ['calendar_date','an'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hn' => 'HN',
            'an' => 'AN',
            'age' => 'อายุ',
            'calendar_date' => 'วันที่',
            'bed' => 'เตียง',
        ];
    }

    public function getPtinfo(){
        return $this->hasOne(Ptinfo::className(),['hn' => 'hn']);
    }


}
