<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ovst".
 *
 * @property int $vn
 * @property string $vstdttm
 * @property int $hn
 * @property string $cln
 * @property string $dct
 * @property string $pttype
 * @property string $sickdate
 * @property string $bw
 * @property string $height
 * @property string $tt
 * @property int $pr
 * @property int $rr
 * @property int $sbp
 * @property int $dbp
 * @property int $an
 * @property string $register
 * @property string $waist_cm
 */
class Ovst extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ovst';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vstdttm', 'sickdate'], 'safe'],
            [['hn', 'pttype', 'ovstist', 'ovstost', 'bw', 'height', 'tt', 'pr', 'rr', 'sbp', 'dbp', 'an', 'register', 'waist_cm'], 'required'],
            [['hn', 'ovstost', 'pr', 'rr', 'sbp', 'dbp', 'an'], 'integer'],
            [['bw', 'height', 'tt', 'waist_cm'], 'number'],
            [['cln', 'dct'], 'string', 'max' => 5],
            [['pttype', 'register'], 'string', 'max' => 2],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'vstdttm' => 'วันที่มารับบริการ',
            'hn' => 'HN',
            'cln' => 'Clinic',
            'dct' => 'Doctor',
            'pttype' => 'สิทธิ์',
            'sickdate' => 'วันที่เริ่มป่วย',
            'ovstost' => 'สถานะกลับบ้าน',
            'bw' => 'น้ำหนัก(kg)',
            'height' => 'ส่วนสูง(cm)',
            'tt' => 'Temp (C)',
            'pr' => 'อัตราการเต้นของหัวใจ',
            'rr' => 'อัตราการหายใจ',
            'sbp' => 'ความดัน Systolic',
            'dbp' => 'ความดัน Diasystolic',
            'an' => 'AN',
            'register' => 'ผู้ลงทะเบียน',
            'waist_cm' => 'รอบเอว (cm)',
        ];
    }
}
