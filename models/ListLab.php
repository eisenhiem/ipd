<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ListLab extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'list_lab';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an', 'fcbc','lcbc','lelyte','felyte','fcreat','lcreat'], 'integer'],
            [['fdate_cbc','ldate_cbc','fdate_elyte','ldate_elyte','fdate_creat','ldate_creat'], 'safe'],            
        ];
    }

    public static function primaryKey(){
        return ['an'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'AN',
            'fcbc' => 'First CBC',
            'lcbc' => 'Last CBC',
            'felyte' => 'First Electrolyte',
            'lelyte' => 'Last Electrolyte',
            'fcreat' => 'First Creatinine',
            'lcreat' => 'Last Creatinine',
            'fdate_cbc' => 'First Date CBC',
            'ldate_cbc' => 'Last Date CBC',
            'fdate_elyte' => 'First Date Electrolyte',
            'ldate_elyte' => 'Last Date Electrolyte',
            'fdate_creat' => 'First Date Creatinine',
            'ldate_creat' => 'Last Date Creatinine',
        ];
    }

}
