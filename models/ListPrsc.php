<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ListPrsc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'list_prsc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an', 'qty',], 'integer'],
            [['dname','unit','medusage','xdoseno'], 'safe'],            
        ];
    }

    public static function primaryKey(){
        return ['an'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'AN',
            'qty' => 'จำนวน',
            'dname' => 'Drug Name',
            'unit' => 'Unit',
            'medusage' => 'วิธีการใช้ยา',
        ];
    }

    public function getMeduse(){
        $medusage = Medusage::find()->where(['dosecode'=> $this->medusage])->one();
        if($medusage){
            return $medusage->doseprn1.' '.$medusage->doseprn2;
        }
    }


}
