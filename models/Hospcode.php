<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hospcode".
 *
 * @property string $namehosp
 * @property string $codehosp
 * @property string $specifics
 * @property string $off_id
 * @property string $off_name1
 * @property string $off_name2
 * @property string $minis
 * @property string $off_type
 * @property string $bed
 * @property string $changwat
 * @property string $ampur
 * @property string $tambon
 * @property string $moo
 * @property string $zonetype
 */
class Hospcode extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hospcode';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['namehosp', 'codehosp', 'specifics', 'off_id', 'off_name1', 'off_name2', 'minis', 'off_type', 'bed', 'changwat', 'ampur', 'tambon', 'moo', 'zonetype'], 'required'],
            [['namehosp', 'off_name1'], 'string', 'max' => 100],
            [['codehosp'], 'string', 'max' => 12],
            [['specifics', 'minis', 'off_type', 'changwat', 'ampur', 'tambon', 'moo'], 'string', 'max' => 2],
            [['off_id'], 'string', 'max' => 5],
            [['off_name2'], 'string', 'max' => 60],
            [['bed'], 'string', 'max' => 4],
            [['zonetype'], 'string', 'max' => 1],
            [['off_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'namehosp' => 'Namehosp',
            'codehosp' => 'Codehosp',
            'specifics' => 'Specifics',
            'off_id' => 'Off ID',
            'off_name1' => 'Off Name1',
            'off_name2' => 'Off Name2',
            'minis' => 'Minis',
            'off_type' => 'Off Type',
            'bed' => 'Bed',
            'changwat' => 'Changwat',
            'ampur' => 'Ampur',
            'tambon' => 'Tambon',
            'moo' => 'Moo',
            'zonetype' => 'Zonetype',
        ];
    }
}
