<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doctor_order".
 *
 * @property int $an
 * @property int $prscno
 * @property string $admit_date
 * @property string $admit_time
 * @property string $progressnote
 * @property string $oneday
 * @property string $con
 */
class DoctorOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctor_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an','prscno'], 'integer'],
            [['admit_date', 'admit_time'], 'safe'],
            [['progressnote', 'oneday', 'con'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'prscno' => 'Prsc No',
            'admit_date' => 'Admit Date',
            'admit_time' => 'Admit Time',
            'progressnote' => 'Progressnote',
            'oneday' => 'One Day',
            'con' => 'Con',
        ];
    }
}
