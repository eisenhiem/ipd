<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Cbc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cbc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an','ln'], 'required'],
            [['an','ln', 'hn', 'hct','wbc','pltc','pmn','lym'], 'integer'],
            [['srvdate','srv_time'], 'safe'],
            [['hb'], 'number'],
        ];
    }

    public static function primaryKey(){
        return ['ln'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ln' => 'Lab Number',
            'an' => 'AN',
            'hn' => 'HN',
            'srvdate' => 'Report Date',
            'srv_time' => 'Report Time',
            'hb' => 'Hb',
            'hct' => 'HCT',
            'wbc' => 'WBC',
            'pltc' => 'Plt Count',
            'pmn' => 'Neutrophil',
            'lym' => 'Lym',
        ];
    }

}
