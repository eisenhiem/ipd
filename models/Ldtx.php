<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ldtx".
 *
 * @property int $ln
 * @property string $dtx
 */
class Ldtx extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ldtx';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ln', 'dtx'], 'required'],
            [['ln'], 'integer'],
            [['dtx'], 'string', 'max' => 15],
            [['ln'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ln' => 'Ln',
            'dtx' => 'Dtx',
        ];
    }
}
