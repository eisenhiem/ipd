<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cbc;

/**
 * CbcSearch represents the model behind the search form of `app\models\Cbc`.
 */
class CbcSearch extends Cbc
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an','ln', 'hn', 'hct', 'wbc', 'pltc', 'pmn', 'lym'], 'integer'],
            [['srvdate', 'srv_time', 'hb'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cbc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ln' => $this->ln,
            'an' => $this->an,
            'hn' => $this->hn,
            'srvdate' => $this->srvdate,
            'srv_time' => $this->srv_time,
            'hct' => $this->hct,
            'wbc' => $this->wbc,
            'pltc' => $this->pltc,
            'pmn' => $this->pmn,
            'lym' => $this->lym,
        ]);

        $query->andFilterWhere(['like', 'hb', $this->hb]);

        return $dataProvider;
    }
}
