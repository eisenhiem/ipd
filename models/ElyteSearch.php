<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Elyte;

/**
 * ElyteSearch represents the model behind the search form of `app\models\Elyte`.
 */
class ElyteSearch extends Elyte
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'hn','ln'], 'integer'],
            [['srvdate', 'srv_time', 'k', 'na', 'cl', 'co2'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Elyte::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'an' => $this->an,
            'ln' => $this->ln,
            'hn' => $this->hn,
            'srvdate' => $this->srvdate,
            'srv_time' => $this->srv_time,
        ]);

        $query->andFilterWhere(['like', 'k', $this->k])
            ->andFilterWhere(['like', 'na', $this->na])
            ->andFilterWhere(['like', 'cl', $this->cl])
            ->andFilterWhere(['like', 'co2', $this->co2]);

        return $dataProvider;
    }
}
