<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "visitadvice".
 *
 * @property int $id
 * @property int $vn
 * @property int $an
 * @property string $advice
 */
class Visitadvice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visitadvice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'advice'], 'required'],
            [['vn', 'an'], 'integer'],
            [['advice'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vn' => 'Vn',
            'an' => 'An',
            'advice' => 'Advice',
        ];
    }
}
