<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "xryrqt".
 *
 * @property int $vn
 * @property int $an
 * @property int $hn
 * @property string $vstdate
 * @property int $vsttime
 * @property int $rqttime
 * @property string $xrycode
 * @property int $fu
 * @property int $rgtok
 * @property int $xan
 * @property string $crok
 * @property string $dct
 */
class Xryrqt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'xryrqt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'hn', 'vsttime', 'rqttime', 'xrycode', 'xan', 'crok', 'dct'], 'required'],
            [['vn', 'an', 'hn', 'vsttime', 'rqttime', 'fu', 'rgtok', 'xan'], 'integer'],
            [['vstdate'], 'safe'],
            [['xrycode'], 'string', 'max' => 4],
            [['crok'], 'string', 'max' => 1],
            [['dct'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'an' => 'An',
            'hn' => 'Hn',
            'vstdate' => 'Vstdate',
            'vsttime' => 'Vsttime',
            'rqttime' => 'Rqttime',
            'xrycode' => 'Xrycode',
            'fu' => 'Fu',
            'rgtok' => 'Rgtok',
            'xan' => 'Xan',
            'crok' => 'Crok',
            'dct' => 'Dct',
        ];
    }
}
