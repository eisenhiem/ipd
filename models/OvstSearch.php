<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ovst;

/**
 * OvstSearch represents the model behind the search form of `app\models\Ovst`.
 */
class OvstSearch extends Ovst
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'hn', 'ovstost', 'pr', 'rr', 'sbp', 'dbp', 'an', 'rcptno'], 'integer'],
            [['vstdttm', 'cln', 'dct', 'pttype', 'sickdate', 'register'], 'safe'],
            [['bw', 'height', 'tt', 'waist_cm'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ovst::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vn' => $this->vn,
            'vstdttm' => $this->vstdttm,
            'hn' => $this->hn,
            'sickdate' => $this->sickdate,
            'ovstost' => $this->ovstost,
            'bw' => $this->bw,
            'height' => $this->height,
            'tt' => $this->tt,
            'pr' => $this->pr,
            'rr' => $this->rr,
            'sbp' => $this->sbp,
            'dbp' => $this->dbp,
            'an' => $this->an,
            'rcptno' => $this->rcptno,
            'waist_cm' => $this->waist_cm,
        ]);

        $query->andFilterWhere(['like', 'cln', $this->cln])
            ->andFilterWhere(['like', 'dct', $this->dct])
            ->andFilterWhere(['like', 'pttype', $this->pttype])
            ->andFilterWhere(['like', 'register', $this->register]);

        return $dataProvider;
    }
}
