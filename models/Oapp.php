<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "oapp".
 *
 * @property int $vn
 * @property string $vstdttm
 * @property int $an
 * @property int $hn
 * @property string $vstdate
 * @property int $vsttime
 * @property string $dct
 * @property string $cln
 * @property string $dscrptn
 * @property string $oappst
 * @property string $fudate
 * @property int $futime
 * @property int $fuok
 */
class Oapp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'oapp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'hn', 'vsttime', 'dscrptn', 'oappst', 'futime'], 'required'],
            [['vn', 'an', 'hn', 'vsttime', 'futime', 'fuok'], 'integer'],
            [['vstdttm', 'vstdate', 'fudate'], 'safe'],
            [['dct', 'cln'], 'string', 'max' => 5],
            [['dscrptn'], 'string', 'max' => 25],
            [['oappst'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'vstdttm' => 'Vstdttm',
            'an' => 'An',
            'hn' => 'Hn',
            'vstdate' => 'Vstdate',
            'vsttime' => 'Vsttime',
            'dct' => 'Dct',
            'cln' => 'Cln',
            'dscrptn' => 'Dscrptn',
            'oappst' => 'Oappst',
            'fudate' => 'Fudate',
            'futime' => 'Futime',
            'fuok' => 'Fuok',
        ];
    }
}
