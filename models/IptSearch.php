<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ipt;

/**
 * IptSearch represents the model behind the search form of `app\models\Ipt`.
 */
class IptSearch extends Ipt
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn', 'an', 'hn', 'rgttime', 'daycnt', 'dchtime', 'dchstts', 'dchtype', 'rr', 'pr', 'sbp', 'dbp', 'painscore', 'actlos', 'ot'], 'integer'],
            [['ward', 'rgtdate', 'pttype', 'prediag', 'dchdate', 'dthdiagdct', 'ipdlct', 'ipttype', 'symptmi', 'drg', 'error', 'warning', 'grouper_version'], 'safe'],
            [['bmi', 'bw', 'height', 'tt', 'rw', 'adjrw', 'wtlos'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ipt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'vn' => $this->vn,
            'an' => $this->an,
            'hn' => $this->hn,
            'rgtdate' => $this->rgtdate,
            'rgttime' => $this->rgttime,
            'dchdate' => $this->dchdate,
            'daycnt' => $this->daycnt,
            'dchtime' => $this->dchtime,
            'dchstts' => $this->dchstts,
            'dchtype' => $this->dchtype,
            'bmi' => $this->bmi,
            'bw' => $this->bw,
            'height' => $this->height,
            'tt' => $this->tt,
            'rr' => $this->rr,
            'pr' => $this->pr,
            'sbp' => $this->sbp,
            'dbp' => $this->dbp,
            'painscore' => $this->painscore,
            'rw' => $this->rw,
            'adjrw' => $this->adjrw,
            'actlos' => $this->actlos,
            'wtlos' => $this->wtlos,
            'ot' => $this->ot,
        ]);

        $query->andFilterWhere(['like', 'ward', $this->ward])
            ->andFilterWhere(['like', 'pttype', $this->pttype])
            ->andFilterWhere(['like', 'prediag', $this->prediag])
            ->andFilterWhere(['like', 'dthdiagdct', $this->dthdiagdct])
            ->andFilterWhere(['like', 'ipdlct', $this->ipdlct])
            ->andFilterWhere(['like', 'ipttype', $this->ipttype])
            ->andFilterWhere(['like', 'symptmi', $this->symptmi])
            ->andFilterWhere(['like', 'drg', $this->drg])
            ->andFilterWhere(['like', 'error', $this->error])
            ->andFilterWhere(['like', 'warning', $this->warning])
            ->andFilterWhere(['like', 'grouper_version', $this->grouper_version]);

        return $dataProvider;
    }
}
