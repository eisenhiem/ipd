<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ListIpd;

/**
 * ListIpdSearch represents the model behind the search form of `app\models\ListIpd`.
 */
class ListIpdSearch extends ListIpd
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'hn', 'age', 'sbp', 'dbp', 'pr', 'rr','lcno'], 'integer'],
            [['ptname','cid' ,'cc', 'pi', 'ph', 'pe', 'prediag', 'doctor','prename'], 'safe'],
            [['bw', 'tt', 'height', 'bmi'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ListIpd::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'an' => $this->an,
            'hn' => $this->hn,
            'cid' => $this->cid,
            'age' => $this->age,
            'bw' => $this->bw,
            'tt' => $this->tt,
            'sbp' => $this->sbp,
            'dbp' => $this->dbp,
            'pr' => $this->pr,
            'rr' => $this->rr,
            'height' => $this->height,
            'bmi' => $this->bmi,
            'lcno' => $this->lcno,
        ]);

        $query->andFilterWhere(['like', 'ptname', $this->ptname])
            ->andFilterWhere(['like', 'prename', $this->prename])
            ->andFilterWhere(['like', 'cc', $this->cc])
            ->andFilterWhere(['like', 'pi', $this->pi])
            ->andFilterWhere(['like', 'ph', $this->ph])
            ->andFilterWhere(['like', 'pe', $this->pe])
            ->andFilterWhere(['like', 'prediag', $this->prediag])
            ->andFilterWhere(['like', 'doctor', $this->doctor]);

        return $dataProvider;
    }
}
