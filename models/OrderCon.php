<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_con".
 *
 * @property int $prscno
 * @property string $namedrug
 * @property string $meduse
 */
class OrderCon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_con';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prscno', 'namedrug'], 'required'],
            [['prscno'], 'integer'],
            [['namedrug'], 'string', 'max' => 50],
            [['meduse'], 'string', 'max' => 60],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'prscno' => 'Prscno',
            'namedrug' => 'Namedrug',
            'meduse' => 'Meduse',
        ];
    }
}
