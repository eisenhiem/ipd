<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order_exten".
 *
 * @property int $prscno
 * @property string $proc
 * @property string $labs
 * @property string $xrays
 */
class OrderExten extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_exten';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prscno'], 'required'],
            [['prscno'], 'integer'],
            [['proc', 'labs', 'xrays'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'prscno' => 'Prscno',
            'proc' => 'Proc',
            'labs' => 'Labs',
            'xrays' => 'Xrays',
        ];
    }
}
