<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reciept".
 *
 * @property int $vn
 * @property string $sum(amnt)
 */
class Reciept extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reciept';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn'], 'required'],
            [['vn'], 'integer'],
            [['total'], 'number'],
        ];
    }

    public static function primaryKey(){
        return ['vn'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'total' => 'Total',
        ];
    }
}
