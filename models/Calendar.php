<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calendar".
 *
 * @property string $calendar_date
 */
class Calendar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['calendar_date'], 'required'],
            [['calendar_date'], 'safe'],
            [['calendar_date'], 'unique'],
        ];
    }
    
    public static function primaryKey(){
        return ['calendar_date'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'calendar_date' => 'Calendar Date',
        ];
    }
    public function getThaiDate($date){
        $months = ['มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $thyear = substr($date,0,4)+543;
        $thmonth = $months[substr($date,6,2)-1];
        $thday = intVal(substr($date,8,2));
        return $thday.' '.$thmonth.' พ.ศ. '.$thyear; 
    }

    public function getDateorder(){
        return $this->getThaiDate($this->calendar_date);
    }

}
