<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Ipt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ipt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['hn', 'vn' ], 'integer'],
            [['rgtdate'], 'safe'],
            [['pttype'],'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'AN',
            'hn' => 'HN',
            'vn' => 'Visit Number',
            'rgtdate' => 'admit date',
            'pttype' => 'Pt Type',
        ];
    }

}
