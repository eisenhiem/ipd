<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Lbuncr extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lbuncr';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ln'], 'required'],
            [['ln',], 'integer'],
            [['bun','creat'], 'safe'],            
        ];
    }

    public static function primaryKey(){
        return ['ln'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ln' => 'Lab No',
            'bun' => 'BUN',
            'creat' => 'Creatinine',
        ];
    }

}
