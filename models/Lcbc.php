<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Lcbc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lcbc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ln'], 'required'],
            [['hct','wbc','pltc','pmn','lym'], 'integer'],
            [['hb'], 'number'],            
        ];
    }

    public static function primaryKey(){
        return ['ln'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ln' => 'Lab No',
            'hb' => 'Hb',
            'hct' => 'HCT',
            'wbc' => 'WBC',
            'pltc' => 'Plt Count',
            'pmn' => 'Neutrophil',
            'lym' => 'Lym',
        ];
    }

}
