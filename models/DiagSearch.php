<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Diag;

/**
 * DiagSearch represents the model behind the search form of `app\models\Diag`.
 */
class DiagSearch extends Diag
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'integer'],
            [['pdx_code', 'pdx_name', 'comb_code', 'comb_name', 'comp_code', 'comp_name', 'other_code', 'other_name', 'ex_code', 'ex_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Diag::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'an' => $this->an,
        ]);

        $query->andFilterWhere(['like', 'pdx_code', $this->pdx_code])
            ->andFilterWhere(['like', 'pdx_name', $this->pdx_name])
            ->andFilterWhere(['like', 'comb_code', $this->comb_code])
            ->andFilterWhere(['like', 'comb_name', $this->comb_name])
            ->andFilterWhere(['like', 'comp_code', $this->comp_code])
            ->andFilterWhere(['like', 'comp_name', $this->comp_name])
            ->andFilterWhere(['like', 'other_code', $this->other_code])
            ->andFilterWhere(['like', 'other_name', $this->other_name])
            ->andFilterWhere(['like', 'ex_code', $this->ex_code])
            ->andFilterWhere(['like', 'ex_name', $this->ex_name]);

        return $dataProvider;
    }
}
