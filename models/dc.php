<?php

namespace app\models;

use Yii;
use yii\base\Model;


class Dc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an'], 'required'],
            [['an', 'hn', 'age','losd',], 'integer'],
            [['ptname','cid','ward','doctor','lcno', 'sex','brthdate', 'nation', 'religion', 'pt_type', 'address','hometel','married_status','job','relation_person','relation','relate_addr','infmtel','bed','rgtdate','rg_time','dchdate','dc_time','refer_hosp','refer_cause','follow_up','dc_type','dc_status'], 'safe'],
            [['bw'], 'number'],
            
        ];
    }

    public static function primaryKey(){
        return ['an'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'AN',
            'hn' => 'HN',
            'cid' => 'CID',
            'brthdate' => 'Date of Birth',
            'age' => 'Age',
            'ptname' => 'Patient Name',
            'bw' => 'Body Weight',
            'prename' => 'Pname',
            'sex' => 'Sex',
            'nation' => 'Nationality',
            'pt_type' => 'PT Type',
            'address' => 'Address',
            'hometel' => 'Tel',
            'married_status' => 'Married Status',
            'job' => 'Occupation',
            'relate_person' => 'Relation Person',
            'relation' => 'Relation',
            'relate_addr' => 'Relation Address',
            'infmtel' => 'Relation Telephone',
            'ward' => 'Ward',
            'bed' => 'BED',
            'rgtdate' => 'Admit Date',
            'rg_time' => 'Admit Time',
            'dchdate' => 'Discharge Date',
            'dc_time' => 'Discharge Time',
            'losd' => 'Length of stay',
            'refer_hosp' => 'Transfer to',
            'refer_cause' => 'Cause',
            'follow_up' => 'Follow Up',
            'religion' => 'Religion',
            'doctor' => 'Doctor',
            'lcno' => 'License No',
            'dc_type' => 'D/C Type',
            'dc_status' => 'D/C Status',
        ];
    }

    public function getPrsc(){
        return $this->hasMany(ListPrsc::className(),['an' => 'an']);
    }

}
