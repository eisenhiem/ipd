<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Lelyte extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lelyte';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ln'], 'required'],
            [['ln',], 'integer'],
            [['k','co2','cl','na'], 'safe'],            
        ];
    }

    public static function primaryKey(){
        return ['ln'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ln' => 'Lab No',
            'k' => 'K',
            'cl' => 'Cl',
            'na' => 'Na',
            'co2' => 'CO2',
        ];
    }

}
