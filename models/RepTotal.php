<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rep_total".
 *
 * @property int $vn
 * @property string $sum(rcptamt)
 */
class RepTotal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rep_total';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn'], 'required'],
            [['vn'], 'integer'],
            [['total'], 'number'],
        ];
    }

    public static function primaryKey(){
        return ['vn'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'total' => 'Total',
        ];
    }
}
