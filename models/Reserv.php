<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reserv".
 *
 * @property int $id
 * @property string $cid
 * @property string $pcu
 * @property int $hn
 */
class Reserv extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reserv';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cid', 'pcu'], 'required'],
            [['hn'], 'integer'],
            [['cid'], 'string', 'max' => 13],
            [['pcu'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cid' => 'Cid',
            'pcu' => 'Pcu',
            'hn' => 'Hn',
        ];
    }
}
