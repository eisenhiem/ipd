<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge".
 *
 * @property int $hn
 * @property int $an
 * @property string $fname
 * @property string $lname
 * @property string $dchdate
 * @property string $link
 */
class Charge extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charge';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hn', 'an'], 'required'],
            [['hn', 'an'], 'integer'],
            [['dchdate'], 'safe'],
            [['fname', 'lname'], 'string', 'max' => 100],
            [['link'], 'string', 'max' => 200],
        ];
    }

    public static function primaryKey(){
        return ['an'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hn' => 'Hn',
            'an' => 'An',
            'fname' => 'ชื่อ',
            'lname' => 'สกุล',
            'dchdate' => 'วันจำหน่าย D/C',
            'link' => 'Link',
        ];
    }
}
