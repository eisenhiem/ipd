<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ph".
 *
 * @property int $vn
 * @property string $ph
 */
class Ph extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ph';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['vn'], 'required'],
            [['vn'], 'integer'],
            [['ph'], 'string'],
        ];
    }

    public static function primaryKey(){
        return ['vn'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'vn' => 'Vn',
            'ph' => 'Ph',
        ];
    }
}
