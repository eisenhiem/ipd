<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ptinfo".
 *
 * @property int $hn
 * @property int $age
 * @property string $prename
 * @property string $ptname
 * @property string $cid
 * @property string $birth
 * @property string $sex
 * @property string $address
 */
class Ptinfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ptinfo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hn','age'], 'integer'],
            [['cid', 'sex'], 'required'],
            [['birth'], 'safe'],
            [['prename'], 'string', 'max' => 15],
            [['ptname'], 'string', 'max' => 51],
            [['cid'], 'string', 'max' => 13],
            [['sex'], 'string', 'max' => 40],
            [['address'], 'string', 'max' => 124],
        ];
    }

    public static function primaryKey(){
        return ['hn'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hn' => 'HN',
            'age' => 'อายุ',
            'prename' => 'คำนำหน้า',
            'ptname' => 'ชื่อ-สกุล',
            'cid' => 'Cid',
            'birth' => 'วันเกิด',
            'sex' => 'เพศ',
            'address' => 'ที่อยู่',
        ];
    }
}
