<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ampur".
 *
 * @property string $nameampur
 * @property string $chwpart
 * @property string $amppart
 */
class Ampur extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ampur';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nameampur', 'chwpart', 'amppart'], 'required'],
            [['nameampur'], 'string', 'max' => 20],
            [['chwpart', 'amppart'], 'string', 'max' => 2],
            [['chwpart', 'amppart'], 'unique', 'targetAttribute' => ['chwpart', 'amppart']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nameampur' => 'Nameampur',
            'chwpart' => 'Chwpart',
            'amppart' => 'Amppart',
        ];
    }
}
