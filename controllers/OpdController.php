<?php

namespace app\controllers;

use Yii;
use app\models\Ovst;
use app\models\OvstSearch;
use app\models\Ptinfo;
use app\models\Pttype;
use yii\web\Controller;

use app\models\Pi;
use app\models\Pe;
use app\models\Symptm;
use app\models\Ph;
use app\models\Clinic;
use app\models\Labresult;
use app\models\Visitadvice;
use app\models\Ovstdx;
use app\models\Oprt;
use app\models\Prsc;
use app\models\Prscdt;
use app\models\Lbbk;
use app\models\Xryrqt;
use app\models\Oapp;
use app\models\Ovstost;
use app\models\Orfro;
use app\models\Reserv;
use app\models\Visitnoteconsult;
use app\models\LCr;
use app\models\Lbuncr;
use app\models\PtAllergy;
use app\models\Reciept;
use app\models\RepTotal;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use yii\filters\AccessControl;
use yii\i18n\Formatter;

/**
 * OpdController implements the CRUD actions for Ovst model.
 */
class OpdController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Ovst models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OvstSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Ovst models.
     * @return mixed
     */
    public function actionRep($id)
    {
        $model = $this->findModel($id);
        $ptinfo = Ptinfo::findOne($model->hn);
        $pttype = Pttype::findOne($model->pttype);
        $rep = RepTotal::findOne($id);
        $paid = Reciept::findOne($id);
        $total = number_format($rep->total - $paid->total,2);
        
        $content = $this->renderPartial('_print_rep',[
            'model' => $model,
            'ptinfo' => $ptinfo,
            'pttype' => $pttype,
            'total' => $total,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'ใบติดตามหนี้'],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    /**
     * Displays a single Ovst model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ovst model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ovst();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vn]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ovst model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->vn]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ovst model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionPrint($id)
    {
        $model = $this->findModel($id);
        $ptinfo = Ptinfo::findOne($model->hn);
        $pttype = Pttype::findOne($model->pttype);

        $cc = Symptm::find()->where(['vn' =>$model->vn])->one();
        $pi = Pi::findOne($model->vn);
        $pe = Pe::findOne($model->vn);
        $ph = Ph::findOne($model->vn);
        $clinic = Clinic::findOne($model->cln);
        $advice = Visitadvice::find()->where(['vn' => $model->vn])->one();
        $diag = Ovstdx::find()->where(['vn' => $model->vn])->orderBy('id')->all();
        $proc = Oprt::find()->where(['vn' => $model->vn])->orderBy('id')->all();
        $prsc = Prsc::find()->where(['vn' => $model->vn])->one();
        $rx = Prscdt::find()->where(['prscno' => $prsc->prscno])->all();
        $lab = Lbbk::find()->where(['vn' => $model->vn])->all();
        $xry = Xryrqt::find()->where(['vn' => $model->vn])->all();
        $appoint = Oapp::find()->where(['vn' => $model->vn])->all();
        $out = Ovstost::find()->where(['ovstost' => $model->ovstost])->one();
        $refer = Orfro::find()->where(['vn' => $model->vn])->one();
        $consult = Visitnoteconsult::find()->where(['vn' => $model->vn])->one();
        $cr = LCr::findOne($model->hn);
        $labresult = Labresult::find()->where(['ln' => $cr->last_ln])->all();

        $content = $this->renderPartial('_print',[
            'model' => $model,
            'ptinfo' => $ptinfo,
            'pttype' => $pttype,
            'cc' => $cc,
            'pi' => $pi,
            'pe' => $pe,
            'ph' => $ph,
            'clinic' => $clinic,
            'advice' => $advice,
            'diag' => $diag,
            'proc' => $proc,
            'rx' => $rx,
            'lab' => $lab,
            'xry' => $xry,
            'appoint' => $appoint,
            'out' => $out,
            'refer' => $refer,
            'consult' => $consult,
            'cr' => $cr,
            'labresult' => $labresult,
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'OPD CARD : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionNcd($id)
    {
        $model = $this->findModel($id);
        $ptinfo = Ptinfo::findOne($model->hn);
        $pttype = Pttype::findOne($model->pttype);

        $cc = Symptm::find()->where(['vn' =>$model->vn])->one();
        $pi = Pi::findOne($model->vn);
        $pe = Pe::findOne($model->vn);
        $ph = Ph::findOne($model->vn);
        $clinic = Clinic::findOne($model->cln);
        $advice = Visitadvice::find()->where(['vn' => $model->vn])->one();
        $diag = Ovstdx::find()->where(['vn' => $model->vn])->orderBy('id')->all();
        $proc = Oprt::find()->where(['vn' => $model->vn])->orderBy('id')->all();
        $prsc = Prsc::find()->where(['vn' => $model->vn])->one();
        $rx = Prscdt::find()->where(['prscno' => $prsc->prscno])->all();
        $lab = Lbbk::find()->where(['vn' => $model->vn])->all();
        $xry = Xryrqt::find()->where(['vn' => $model->vn])->all();
        $appoint = Oapp::find()->where(['vn' => $model->vn])->all();
        $out = Ovstost::find()->where(['ovstost' => $model->ovstost])->one();
        $refer = Orfro::find()->where(['vn' => $model->vn])->one();
        $consult = Visitnoteconsult::find()->where(['vn' => $model->vn])->one();
        $cr = LCr::findOne($model->hn);
        $labresult = Labresult::find()->where(['ln' => $cr->last_ln])->all();
        $allergy = PtAllergy::find()->where(['hn'=>$model->hn])->one();

        $content = $this->renderPartial('_print_ncd',[
            'model' => $model,
            'ptinfo' => $ptinfo,
            'pttype' => $pttype,
            'cc' => $cc,
            'pi' => $pi,
            'pe' => $pe,
            'ph' => $ph,
            'clinic' => $clinic,
            'advice' => $advice,
            'diag' => $diag,
            'proc' => $proc,
            'rx' => $rx,
            'lab' => $lab,
            'xry' => $xry,
            'appoint' => $appoint,
            'out' => $out,
            'refer' => $refer,
            'consult' => $consult,
            'cr' => $cr,
            'labresult' => $labresult,
            'allergy' => $allergy,
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => [148,210],//Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'ประวัติผู้ป่วยโรคไม่ติดต่อเรื้อรัง : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    /**
     * Lists all Ovst models.
     * @return mixed
     */
    public function actionVacc()
    {
        $model = Reserv::find()->all();
        
        $content = $this->renderPartial('_print_vacc',[
            'model' => $model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'ใบทะเบียนฉีดวัคซีน'],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    /**
     * Finds the Ovst model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ovst the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ovst::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public static function get_cc($vn){
        $select = "SELECT symptom as cc from hi.symptm WHERE symptm.vn=".$vn;
        $query = Yii::$app->db->createCommand($select)->queryAll();
        return $query;
    }

    public static function get_pi($vn){
        $select = "SELECT group_concat(pillness) as pi from hi.pillness WHERE pillness.vn=".$vn." ORDER BY id";
        $query = Yii::$app->db->createCommand($select)->queryAll();
        return $query;
    }

    public static function get_pe($vn){
        $select = "SELECT group_concat(sign) as pe from hi.sign WHERE sign.vn=".$vn." ORDER BY id";
        $query = Yii::$app->db->createCommand($select)->queryAll();
        return $query;
    }

    public static function get_ph($vn){
        $select = "SELECT group_concat(phistory) as ph from hi.phistory WHERE phistory.vn=".$vn." ORDER BY id";
        $query = Yii::$app->db->createCommand($select)->queryAll();
        return $query;
    }

    public static function get_pdx($vn){
        $select = "SELECT icd10 as pdx,icd10name as namediag from hi.ovstdx WHERE cnt=1 and ovstdx.vn=".$vn;
        $query = Yii::$app->db->createCommand($select)->queryAll();
        return $query;
    }

}
