<?php

namespace app\controllers;

use Yii;
use app\models\Ipt;
use app\models\Cbc;
use app\models\Elyte;
use app\models\Dc;
use app\models\DcSearch;
use app\models\IptSearch;
use app\models\ListIpd;
use app\models\ListIpdSearch;
use app\models\ListPrsc;
use app\models\ListPrscSearch;
use app\models\ListLab;
use app\models\Labresult;
use app\models\LCr;
use app\models\Lcbc;
use app\models\Lelyte;
use app\models\Lbuncr;
use app\models\ViewDtx;
use app\models\ViewDtxSearch;
use app\models\Diag;
use app\models\IpdPe;
use app\models\Lbbk;
use app\models\Proc;
use app\models\NurseNote;
use app\models\DoctorOrder;
use app\models\PtAllergy;
use app\models\Ptinfo;
use app\models\Viewfood;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use yii\filters\AccessControl;

//use yii\data\SqlDataProvider;

/**
 * IpdController implements the CRUD actions for Ipt model.
 */
class IpdController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Ipt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ListIpdSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['an'=>SORT_DESC]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Ipt models.
     * @return mixed
     */
    public function actionDc()
    {
        $searchModel = new DcSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->orderBy(['an'=>SORT_DESC]);

        return $this->render('index_dc', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Ipt model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionFood()
    {
        $model = Viewfood::find()->all();

        $content = $this->renderPartial('_food',[
            'model' => $model,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'ใบเบิกอาหาร '],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        return $pdf->render();

    }

    /**
     * Creates a new Ipt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ipt();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->an]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Ipt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->an]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Ipt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ipt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ipt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ListIpd::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPrint($id)
    {
        $model = $this->findModel($id);
        $ptinfo = Ptinfo::find()->where(['hn'=>$model->hn])->one();
        $allergy = PtAllergy::find()->where(['hn'=>$model->hn])->one();
        $lablist = Lbbk::find()->where(['an'=>$model->an])->all();
        $cbc = Cbc::find()->where(['an' => $model->an])->one();
        $elyte = Elyte::find()->where(['an' => $model->an])->one();
        $cr = LCr::findOne($model->hn);
        $creat = new Labresult();
        $nr = NurseNote::find()->where(['an'=>$model->an])->all();
        $doctor_order = DoctorOrder::find()->where(['an'=>$model->an])->all();
        
        if($cr){
            $creat = Labresult::find()->where(['ln' =>$cr->last_ln])->one();
        }

        $content = $this->renderPartial('_print',[
            'model' => $model,
            'cbc' => $cbc,
            'elyte' => $elyte,
            'cr'=> $cr,
            'creat'=> $creat,
            'lablist' => $lablist,
            'nr' => $nr,
            'doctor_order' => $doctor_order,
            'allergy' => $allergy,
            'ptinfo' => $ptinfo,
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Admission Note : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                'SetFooter'=>['<h6 align="right">ปรับปรุงเมื่อวันที่ 30 ธ.ค. 2564</h6>'],
            ]
        ]);

        return $pdf->render();

    }

    public function actionDcprint($id)
    {
        $model = Dc::findOne($id);

        $diag = new Diag;
        $diag = Diag::findOne($id);

        $proc = new Proc;
        $proc = Proc::findOne($id);

        $ipd_pe = new IpdPe;
        $ipd_pe = IpdPe::findOne($id);

        $lablist = new ListLab;
        $lablist = ListLab::findOne($id);

        $fcbc = new Cbc;
        $lcbc = new Cbc;

        $felyte = new Elyte;
        $lelyte = new Elyte;

        $fcreat = new Labresult;
        $lcreat = new Labresult;

        $dtxSearch = new ViewDtxSearch;
        $dtx = $dtxSearch->search(Yii::$app->request->queryParams);
        $dtx->query->where(['an'=>$id]);

        if($lablist) {
            if($lablist->fcbc){
                if($lablist->fcbc <> $lablist->lcbc) {
                    $fcbc->ln = $lablist->fcbc;
                    $fcbc->hb = Labresult:: find()->where(['ln' => $lablist->fcbc , 'lab_code_local' => 'Hb'])->one();
                    $fcbc->hct = Labresult:: find()->where(['ln' => $lablist->fcbc , 'lab_code_local' => 'Hct'])->one();
                    $fcbc->pltc = Labresult:: find()->where(['ln' => $lablist->fcbc , 'lab_code_local' => 'Pltc'])->one();
                    $fcbc->pmn = Labresult:: find()->where(['ln' => $lablist->fcbc , 'lab_code_local' => 'Pmn'])->one();
                    $fcbc->lym = Labresult:: find()->where(['ln' => $lablist->fcbc , 'lab_code_local' => 'Lym'])->one();
                    $lcbc->ln = $lablist->lcbc;
                    $lcbc->hb = Labresult:: find()->where(['ln' => $lablist->lcbc , 'lab_code_local' => 'Hb'])->one();
                    $lcbc->hct = Labresult:: find()->where(['ln' => $lablist->lcbc , 'lab_code_local' => 'Hct'])->one();
                    $lcbc->pltc = Labresult:: find()->where(['ln' => $lablist->lcbc , 'lab_code_local' => 'Pltc'])->one();
                    $lcbc->pmn = Labresult:: find()->where(['ln' => $lablist->lcbc , 'lab_code_local' => 'Pmn'])->one();
                    $lcbc->lym = Labresult:: find()->where(['ln' => $lablist->lcbc , 'lab_code_local' => 'Lym'])->one();
                } else {
                    $fcbc->ln = $lablist->fcbc;
                    $fcbc->hb = Labresult:: find()->where(['ln' => $lablist->fcbc , 'lab_code_local' => 'Hb'])->one();
                    $fcbc->hct = Labresult:: find()->where(['ln' => $lablist->fcbc , 'lab_code_local' => 'Hct'])->one();
                    $fcbc->pltc = Labresult:: find()->where(['ln' => $lablist->fcbc , 'lab_code_local' => 'Pltc'])->one();
                    $fcbc->pmn = Labresult:: find()->where(['ln' => $lablist->fcbc , 'lab_code_local' => 'Pmn'])->one();
                    $fcbc->lym = Labresult:: find()->where(['ln' => $lablist->fcbc , 'lab_code_local' => 'Lym'])->one();
                }
            }
            if($lablist->felyte){
                if($lablist->felyte == $lablist->lelyte) {
                    $felyte->ln = $lablist->felyte;
                    $felyte->k = Labresult:: find()->where(['ln' => $lablist->felyte , 'lab_code_local' => 'K'])->one();
                    $felyte->na = Labresult:: find()->where(['ln' => $lablist->felyte , 'lab_code_local' => 'Na'])->one();
                    $felyte->cl = Labresult:: find()->where(['ln' => $lablist->felyte , 'lab_code_local' => 'Cl'])->one();
                    $felyte->co2 = Labresult:: find()->where(['ln' => $lablist->felyte , 'lab_code_local' => 'Co2'])->one();
                } else {
                    $felyte->ln = $lablist->felyte;
                    $felyte->k = Labresult:: find()->where(['ln' => $lablist->felyte , 'lab_code_local' => 'K'])->one();
                    $felyte->na = Labresult:: find()->where(['ln' => $lablist->felyte , 'lab_code_local' => 'Na'])->one();
                    $felyte->cl = Labresult:: find()->where(['ln' => $lablist->felyte , 'lab_code_local' => 'Cl'])->one();
                    $felyte->co2 = Labresult:: find()->where(['ln' => $lablist->felyte , 'lab_code_local' => 'Co2'])->one();
                    $lelyte->ln = $lablist->lelyte;
                    $lelyte->k = Labresult:: find()->where(['ln' => $lablist->lelyte , 'lab_code_local' => 'K'])->one();
                    $lelyte->na = Labresult:: find()->where(['ln' => $lablist->lelyte , 'lab_code_local' => 'Na'])->one();
                    $lelyte->cl = Labresult:: find()->where(['ln' => $lablist->lelyte , 'lab_code_local' => 'Cl'])->one();
                    $lelyte->co2 = Labresult:: find()->where(['ln' => $lablist->lelyte , 'lab_code_local' => 'Co2'])->one();
                }        
            }    
            if($lablist->fcreat){
                if($lablist->fcreat == $lablist->lcreat) {
                    $fcreat = Labresult:: find()->where(['ln' => $lablist->fcreat , 'lab_code_local' => 'Creat'])->one();
                } else {
                    $fcreat = Labresult:: find()->where(['ln' => $lablist->fcreat  , 'lab_code_local' => 'Creat'])->one();
                    $lcreat = Labresult:: find()->where(['ln' => $lablist->lcreat  , 'lab_code_local' => 'Creat'])->one();
                }        
            }    
        }

        $searchModel = new ListPrscSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['an'=> $id]);

        $content = $this->renderPartial('_dc',[
            'model' => $model,
            'lablist' => $lablist,
            'proc' => $proc,
            'dataProvider' => $dataProvider,
            'fcbc' => $fcbc,
            'lcbc' => $lcbc,
            'felyte' => $felyte,
            'lelyte' => $lelyte,
            'fcreat' => $fcreat,
            'lcreat' => $lcreat,
            'diag' =>$diag,
            'ipd_pe' => $ipd_pe,
//            'dtx' => $dtx,
        ]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@app/web/css/pdf.css',
            // any css to be embedded if required
            'cssInline' => '.bd{border:1.5px solid; text-align: center;} .ar{text-align:right} .imgbd{border:1px solid}',
            // set mPDF properties on the fly
            'options' => ['title' => 'Discharge Summary : '.$id],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>[''],
                'SetFooter'=>['<h6 align="right">ปรับปรุงเมื่อวันที่ 30 ธ.ค. 2564</h6>'],
            ]
        ]);

        return $pdf->render();
    }

}
