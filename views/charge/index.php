<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ChargeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ค้นหาชาร์ต';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charge-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'hn',
            'an',
            'fname',
            'lname',
            'dchdate',
            [
                'label'=>'ชาร์ตเดิม',
                'format'=>'raw',
                'value'=>function($model){
                  return Html::a('ชาร์ต '.$model->an,$model->link,['class' => 'btn btn-info','target'=>'_blank']);
                }
            ],
        ],
    ]); ?>


</div>
