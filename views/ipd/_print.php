<?php

use app\models\Ampur;
use app\models\Ipt;
use yii\helpers\Html;
use app\models\OrderOneday;
use app\models\OrderCon;
use app\models\OrderExten;
use app\models\Pttype;
use app\models\Setup;

$hosp = Setup::find()->one();
$amp = Ampur::find()->where(['chwpart' => $hosp->hi_chw_cd, 'amppart' => $hosp->hi_amp_cd])->one();
$amp_name = str_replace('อ.', 'อำเภอ', $amp->nameampur);
$ipt = Ipt::find()->where(['an' => $model->an])->one();
$pttype = Pttype::find()->where(['pttype' => $ipt->pttype ])->one();

function egfr($age, $creat, $sex)
{
  if ($creat) {
    if ($sex == '1') {
      if ($creat > 0.9) {
        $egfr = 141  * pow(0.993, $age) / pow(($creat / 0.9), 1.209);
      } else {
        $egfr = 141  * pow(0.993, $age) / pow(($creat / 0.9), 0.411);
      }
    } else {
      if ($creat > 0.9) {
        $egfr = 144  * pow(0.993, $age) / pow(($creat / 0.7), 1.209);
      } else {
        $egfr = 144  * pow(0.993, $age) / pow(($creat / 0.7), 0.329);
      }
    }
  } else {
    $egfr = 'NA';
  }
  return round($egfr, 2);
}

function thaimonth($m)
{
  switch ($m) {
    case '01':
      $thainame = 'มกราคม';
      break;
    case '02':
      $thainame = 'กุมภาพันธ์';
      break;
    case '03':
      $thainame = 'มีนาคม';
      break;
    case '04':
      $thainame = 'เมษายน';
      break;
    case '05':
      $thainame = 'พฤษภาคม';
      break;
    case '06':
      $thainame = 'มิถุนายน';
      break;
    case '07':
      $thainame = 'กรกฎาคม';
      break;
    case '08':
      $thainame = 'สิงหาคม';
      break;
    case '09':
      $thainame = 'กันยายน';
      break;
    case '10':
      $thainame = 'ตุลาคม';
      break;
    case '11':
      $thainame = 'พฤศจิกายน';
      break;
    case '12':
      $thainame = 'ธันวาคม';
      break;
  }
  return $thainame;
}
?>
<table width="100%" cellpadding="2" cellspacing="0">
  <tr>
    <td align="center">
      <h2>Admission Note</h2>
      <h3>โรงพยาบาล<?= $hosp->hi_hsp_nm ?> <?= $amp_name ?> จังหวัด<?= $hosp->hi_chw_nm ?></h3>
    </td>
  </tr>
</table>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
  <tr>
    <td>
      &emsp;<b>HN: <?php echo $model->hn; ?></b>
      &emsp;<b>AN: <?php echo $model->an; ?></b>
      &emsp;<b>ชื่อ-สกุล: </b><?php echo $model->prename . $model->ptname; ?>
      &emsp;<b>อายุ </b><?php echo $model->age; ?> ปี
      &emsp;<b>เพศ </b><?php echo $ptinfo->sex; ?>
      &emsp;<b>สิทธิ์ </b><?php echo $pttype->namepttype; ?><br>
      &emsp;<b>Department: </b>ตึกผู้ป่วยใน &emsp;
      &emsp;<b>Bed:</b>
    </td>
  </tr>
  <tr>
    <td>
      <h3>ประวัติ</h3>
      &emsp;<b>Chief Compaint:</b>
      <?php echo $model->cc; ?><br>
      &emsp;<b>Present Illness:</b>
      <?php echo $model->pi; ?><br>
      &emsp;<b>PI</b> (ที่เกี่ยวข้องกับอาการที่มา/ปัจจัยที่ทำให้ต้องได้รับการรักษาในโรงพยาบาล):<br>&emsp;<br>&emsp;<br>
      &emsp;<b>ประวัติเพิ่มเติม</b><br>
      &emsp;&emsp;[&emsp;] ไม่มี &ensp;[&emsp;] มี คนอื่นที่มีอาการแบบเดียวกัน คือ<br>
      &emsp;&emsp;[&emsp;] ไม่ได้ [&emsp;] ได้ รักษาที่อื่นมาก่อน ที่<br>
      &emsp;&emsp;[&emsp;] อุบัติเหตุ สถานที่เกิดเหตุ &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;กิจกรรมขณะเกิดเหตุ
    </td>
  </tr>
  <tr>
    <td>
      <h2>Past History</h2>
      <table width="100%" cellpadding="2" cellspacing="0">
        <tr>
          <td rowspan=8 style="vertical-align:top; width:250">&emsp;<?php echo $model->ph ?></td>
          <td width="80">&emsp;<b>โรคประจำตัว</b></td>
          <td width="60">[&emsp;] ไม่มี </td>
          <td>[&emsp;] มี(ระบุ) </td>
        </tr>
        <tr>
          <td>&emsp;<b>การแพ้ยา</b></td>
          <td>[&emsp;] ไม่แพ้</td>
          <td>[&emsp;] แพ้ยา(ระบุ) </td>
        </tr>
        <tr>
          <td>&emsp;<b>การผ่าตัด</b></td>
          <td>[&emsp;] ไม่เคย</td>
          <td>[&emsp;] เคย(ระบุ) </td>
        </tr>
        <tr>
          <td>&emsp;<b>การสูบบุหรี่</b></td>
          <td>[&emsp;] ไม่สูบ</td>
          <td>[&emsp;] มีสูบ วันละ &emsp; &emsp; มวน สูบมานาน &emsp; &emsp; &emsp;ปี</td>
        </tr>
        <tr>
          <td>&emsp;<b>การดื่นสุรา</b></td>
          <td>[&emsp;] ไม่ดื่ม</td>
          <td>[&emsp;] ดื่มประจำ [&emsp;] ดื่มตามโอกาสงานสำคัญ </td>
        </tr>
        <tr>
          <td colspan=3>&emsp;<b>โรคประจำตัวหรือโรคพันธุกรรมในครอบครัว</b> [&emsp;] ไม่มี [&emsp;] มี(ระบุ)</td>
        </tr>
        <tr>
          <td colspan=3>
            &emsp;<b>การคุมกำเหนิด</b> &emsp;&emsp;&emsp;&emsp;
            <b>LMP</b> &emsp;&emsp;/&emsp;&emsp;/&emsp;&emsp;&emsp; <b>G</b> &emsp; <b>P</b> &emsp; <b>A</b> &emsp; <b>L</b> &emsp; <b>GA</b> &emsp;&emsp; Wks
          </td>
        </tr>
        <tr>
          <td colspan=3>
            &emsp;<b>วัคซีน</b> [&emsp;] ครบ [&emsp;] ไม่ครบ
            <b>พัฒนาการ</b> [&emsp;] ปกติ [&emsp;] ไม่ปกติ
          </td>
        </tr>
        <tr>
          <td colspan=4>
            &emsp;<b>แหล่งที่มาของข้อมูล</b> [&emsp; ] ผู้ป่วย [&emsp; ] ผู้ปกครอง [&emsp; ] ญาติ [&emsp; ] ใบส่งต่อ [&emsp; ] อื่นๆ(ระบุ)
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
  <tr>
    <td colspan=3>
      <h2>Vital Sign</h2>
      &emsp;
      <b>BT:</b> <?php echo $model->tt; ?> ºC
      <b>BP:</b> <?php echo $model->sbp . '/' . $model->dbp; ?> mmHg
      <b>PR:</b> <?php echo $model->pr ?> /m
      <b>RR:</b> <?php echo $model->rr ?> /m
      <b>น้ำหนัก:</b> <?php echo $model->bw ?> kg
      <b>ส่วนสูง:</b> <?php echo $model->height ?> cm
      <b>BMI:</b> <?php echo $model->bmi ?>
    </td>
  </tr>
  <tr>
    <td style="vertical-align:top" width="250">
      <h2>Review of System</h2>
      <table width="100%" cellpadding="2" cellspacing="0">
        <tr>
          <td>HEENT</td>
          <td>[&emsp;] ปกติ [&emsp;] ผิดปกติ </td>
        </tr>
        <tr>
          <td>Heart</td>
          <td>[&emsp;] ปกติ [&emsp;] ผิดปกติ </td>
        </tr>
        <tr>
          <td>Lung</td>
          <td>[&emsp;] ปกติ [&emsp;] ผิดปกติ </td>
        </tr>
        <tr>
          <td>Abdomen</td>
          <td>[&emsp;] ปกติ [&emsp;] ผิดปกติ </td>
        </tr>
        <tr>
          <td>Back&CVA</td>
          <td>[&emsp;] ปกติ [&emsp;] ผิดปกติ </td>
        </tr>
        <tr>
          <td>Extremities & Skin</td>
          <td>[&emsp;] ปกติ [&emsp;] ผิดปกติ </td>
        </tr>
        <tr>
          <td>Neuro. Signs</td>
          <td>[&emsp;] ปกติ [&emsp;] ผิดปกติ </td>
        </tr>
      </table><br>
      <b>อื่นๆ</b>(ถ้ามี)<br>
      &emsp;<br>&emsp;<br>&emsp;<br>
    </td>
    <td style="vertical-align:top" width="250">
      <h2>Physical Examination</h2>
      <table width="100%" cellpadding="2" cellspacing="0">
        <tr>
          <td>HEENT</td>
          <td>[&emsp;] WNL [&emsp;] Abnormal </td>
        </tr>
        <tr>
          <td>Heart</td>
          <td>[&emsp;] WNL [&emsp;] Abnormal </td>
        </tr>
        <tr>
          <td>Lung</td>
          <td>[&emsp;] WNL [&emsp;] Abnormal </td>
        </tr>
        <tr>
          <td>Abdomen</td>
          <td>[&emsp;] WNL [&emsp;] Abnormal </td>
        </tr>
        <tr>
          <td>Back&CVA</td>
          <td>[&emsp;] WNL [&emsp;] Abnormal </td>
        </tr>
        <tr>
          <td>Extremities & Skin</td>
          <td>[&emsp;] WNL [&emsp;] Abnormal </td>
        </tr>
        <tr>
          <td>Neuro. Signs</td>
          <td>[&emsp;] WNL [&emsp;] Abnormal </td>
        </tr>
        <tr>
          <td>General Appearence</td>
          <td>[&emsp;] Healthy [&emsp;] Active </td>
        </tr>
      </table><br>
      <b>PE: </b><?php echo $model->pe ?><br>
      &emsp;<br>&emsp;
    </td>
    <td style="vertical-align:top">
      &emsp;ภาพประกอบการตรวจร่างกาย<br>
      <table width="100%" border="1" cellpadding="2" cellspacing="0">
        <tr>
          <td>
            &emsp;<br>&emsp;<br>&emsp;<br>&emsp;<br>&emsp;<br>&emsp;<br>&emsp;<br>&emsp;<br>&emsp;<br>&emsp;<br>&emsp;<br>&emsp;<br>&emsp;<br>&emsp;<br>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td colspan=3>
      <b>PV</b> &emsp;&emsp;&emsp;&emsp;
      <b>Cx dilate</b> &emsp;&emsp;&emsp;&emsp;cm
      <b>Efface</b> &emsp;&emsp;&emsp;&emsp;%
      <b>Station</b> &emsp;&emsp;&emsp;&emsp;
      <b>Presentation</b> &emsp;&emsp;&emsp;&emsp;
      <b>MI</b> &emsp;&emsp;&emsp;&emsp;
      <b>MR</b> &emsp;&emsp;&emsp;&emsp;
      <b>FHR</b> &emsp;&emsp;&emsp;&emsp;bpm
      <br>&emsp;
    </td>
  </tr>
</table>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
  <tr>
    <td width="50%" valign="top">LAB Finding ผลการชันสูตรที่สำคัญ
      <br>&emsp;
      <?php if ($cr) {
        echo 'Creatinine: ' . $creat->labresult . ' mg/dL ';
        echo 'Date : ' . $cr->last_cr; ?>
        <br>&emsp;
      <?php
        echo 'eGFR(CKD-EPI): ' . egfr($cr->age, $creat->labresult, $cr->sex) . ' mL/min/1.73^2';
      } ?>
    </td>
    <td valign="top" style="background-image:url(../web/imgs/cbc.jpg);background-repeat:no-repeat;background-size:160px 100px;   width: 160px; height: 100px;">
      <?php if ($cbc) {
        echo 'CBC : ' . $cbc->srvdate . ' เวลา ' . $cbc->srv_time ?><br><br>&emsp; <?php echo $cbc->hb; ?>&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; <?php echo $cbc->pmn; ?>
        <br> &emsp; &emsp; &emsp; &emsp;&emsp;<?php echo $cbc->wbc; ?> <br>&emsp;
        <br> &emsp; &emsp; &emsp; &emsp; <?php echo $cbc->pltc; ?> <br>&emsp;
        <br> &emsp; <?php echo $cbc->hct; ?>&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
      <?php echo $cbc->lym;
      } ?>
    </td>
    <td valign="top" style="background-image:url(../web/imgs/elyte.jpg);background-repeat:no-repeat;background-size:160px 100px;   width: 160px; height: 100px;">
      <?php if ($elyte) {
        echo 'Elyte : ' . $elyte->srvdate . ' เวลา ' . $elyte->srv_time ?><br><br>&emsp;&emsp; &emsp;
        <?php echo $elyte->na; ?>&emsp; &emsp; <?php echo $elyte->k; ?>
        <br> &emsp;<br> &emsp;<br>&emsp;&emsp; &emsp;
        <?php echo $elyte->cl; ?>&emsp; &emsp;
      <?php echo $elyte->co2;
      } ?>
    </td>
  </tr>
  <tr>
    <td colspan=3>Previous Diagnosis : <?php echo str_replace(', unspecified','',$model->prediag); ?></td>
  </tr>
  <tr>
    <td colspan=3 valign="top">
      <h2>Plan of Treatment</h2>
      <br>&emsp;<br>&emsp;<br>&emsp;
      &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
      แพทย์ผู้ตรวจร่างกาย<br>&emsp; <br>
      &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
      &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
      <?php if ($model->doctor) {
        echo '(' . $model->doctor . ')';
      } else {
        echo '(..........................................)';
      } ?><br>&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
      <?php if ($model->lcno) {
        echo 'ว.' . $model->lcno;
      } ?>
    </td>
  </tr>
</table>
<pagebreak />
<h2 align="center">ใบรับทราบและยินยอมรับการรักษาโรงพยาบาล<?= $hosp->hi_hsp_nm ?> จังหวัด<?= $hosp->hi_chw_nm ?></h2>
<h4 align="center">วันที่ <?= date('d'); ?> เดือน <?= thaimonth(date('m')); ?> พ.ศ. <?= date('Y') + 543; ?> เวลา <?= date('H:i'); ?> น.</h4>
&emsp; &emsp; &emsp; &emsp; &emsp; ข้าพเจ้า <?= $model->prename . $model->ptname; ?> อายุ <?= $model->age; ?> ปี หรือ _______________________________________ ซึ่งเป็น <br>
[&emsp; ] ผู้ป่วย หรือ [&emsp; ] เป็นผู้แทนผู้ป่วยในฐานะ _________________ ของ __________________________________ อายุ _________ ปี <br>
โดยสมัครใจเข้ารับการรักษาในโรงพยาบาลเหล่าดเสือโก้กและอนุญาตให้คณะแพทย์และทีมสุขภาพทำการรักษาพยาบาลที่เกี่ยวข้องกับการรักษาในครั้งนี้ ก่อนลงนามข้าพเจ้าได้ทราบรายละเอียดดังต่อไปนี้<br>
1.โรคหรือการวินิจฉัยโรคเบื้องต้น ___________________________________ ผู้ป่วย/ญาติสามารถสอบถามเพิ่มเติมกับแพทย์เจ้าของไข้ได้<br>
2.เหตุผลหรือความจำเป็นในการเข้ารับการรักษา/ทำหัตถการ/การผ่าตัดในโรงพยาบาล<br>
[&emsp; ] โรคที่ผู้ป่วยเป็นมีความจำเป็นต้องได้รับการรักษาที่ถูกต้องและถูกวิธี <br>
[&emsp; ] โรคที่ผู้ป่วยเป็นต้องได้รับการรักษาพยาบาลและดูแลใกล้ชิดจากทีมสุขภาพ<br>
[&emsp; ] เพื่อให้ยาปฏิชีวนะลดการติดเชื้อ<br>
[&emsp; ] เพื่อให้เลือดหรือสารทดแทนต่างๆ<br>
[&emsp; ] เหตุผลหรือความจำเป็นอื่นๆ ____________________________________________________________________<br>
3.วิธีการรักษา [&emsp; ] ให้สารน้ำทางหลอดเลือดดำ [&emsp; ] ให้ยารับประทาน [&emsp; ] ให้ยาฉีด [&emsp; ] ทำแผล [&emsp; ] การผ่าตัด <br>
[&emsp; ] อื่นๆ ระบุ _________________________________________________________________________________<br>
4.ระยะเวลาที่คาดว่าจะนอนโรงพยาบาลอย่างน้อย __________ วัน หากไม่มีภาวะแทรกซ้อน<br>
5.ข้อดีของการเข้ารับการรักษาโรค [&emsp; ] ลดภาวะเสี่ยงต่อการติดเชื้อ [&emsp; ] ลดภาวะเสี่ยงต่อการเสียเลือด [&emsp; ] ลดภาวะเสี่ยงต่อภาวะแทรกซ้อน <br>
[&emsp; ] ลดภาวะเสี่ยงต่อการพิการของร่างกาย [&emsp; ] อื่นๆ ______________________________________________<br>
6.ข้อเสียเมื่อไม่ได้รับการรักษาทันที [&emsp; ] อาจทำให้เกิดการติดเชื้อในระบบต่างๆได้ [&emsp; ] อาจทำให้เกิดความพิการหรือเสียชีวิต <br>
[&emsp; ] อาจทำให้เกิดโรครุนแรงมากขึ้นจนไม่สามารถรักษาได้ [&emsp; ] ข้อเสีย อื่นๆ _____________________________<br>
7.อาการผิดปกติหรือภาวะแทรกซ้อนที่อาจเกิดขึ้นได้ในขณะเข้ารับการรักษา [&emsp; ] อาจทำให้เกิดอาการแพ้ยาหรือเลือดที่ใช้ในการรักษา <br>
[&emsp; ] ติดเชื้อในโรงพยาบาลหรือติดเชื้อจากการให้สารน้ำหรือยาทางหลอดเลือดดำ [&emsp; ] อื่นๆ _______________________________________________<br>
&emsp; &emsp; &emsp; &emsp; &emsp; ทั้งนี้ยินยอมให้ส่งผู้ป่วยไปเพื่อรับการตรวจรักษา ณ สถานพยาบาลอื่นเมื่อมีเหตุอันควร หากผู้ป่วยรายนี้ได้รับอันตรายอันเนื่องจากการรักษาพยาบาลตามวรรคแรก ข้าพเจ้าจะไม่เรียกร้องหรือฟ้องร้องดำเนินคดีแก่ทีมคณะแพทย์และทีมสุขภาพแต่อย่างใด เจ้าหน้าที่ของโรงพยาบาลเหล่าเสือโก้ก ได้อธิบายและคำตอบของข้าพเจ้าจนเป็นที่เข้าใจโดยตลอด จึงลงลายมือหรือลายพิมพ์นิ้วมือไว้เป็นหลักฐาน<br>
ระบุสาเหตุที่ผู้ป่วยไม่สามารถลงนามได้ <br> [&emsp; ] ผู้ป่วยไม่รู้สึกตัว [&emsp; ] ผู้ป่วยทารกแรกเกิด [&emsp; ] ไม่บรรลุนิติภาวะ (ต่ำกว่า 18 ปี) [&emsp; ] ผู้บกพร่องทางกายหรือจิต <br>
<br>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td>ลงชื่อ &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; ผู้รับทราบข้อมูล(ผู้ป่วย/ผู้แทน/ญาติ) </td>
    <td>ลงชื่อ &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; แพทย์ผู้รักษา </td>
  </tr>
  <tr>
    <td>( &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; ) เกี่ยวข้องเป็น ______________ </td>
    <td>
      <?php if ($model->doctor) {
        echo '( ' . $model->doctor . ' )';
      } else {
        echo '(..........................................)';
      } ?></td>
  </tr>
  <tr>
    <td>(ถ้าเขียนไม่ได้ให้พิมพ์ลายนิ้วมือ ระบุนิ้ว _____________ ข้าง ____________ )</td>
    <td>&emsp; &emsp; &emsp; &emsp;
      <?php if ($model->lcno) {
        echo 'ว.' . $model->lcno;
      } ?></td>
  </tr>
  <tr>
    <td>&emsp; </td>
    <td>&emsp; </td>
  </tr>
  <tr>
    <td>ลงชื่อ &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; พยานผู้ป่วยเกี่ยวข้องเป็น ___________ </td>
    <td>ลงชื่อ &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; พยานฝ่ายการแพทย์ </td>
  </tr>
  <tr>
    <td>( &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; ) <br>
      [&emsp; ] ผู้ป่วย/ญาติ มาคนเดียว <br><br>
      เบอร์โทรศัพท์ _________________ <br><br>
      รับทราบข้อมูลเมื่อวันที่ <?= date('d'); ?> เดือน <?= thaimonth(date('m')); ?> พ.ศ. <?= date('Y') + 543; ?> เวลา ___ : ___ น.
    </td>
    <td>( &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; ) <br><br><br><br><br>วันที่ <?= date('d'); ?> เดือน <?= thaimonth(date('m')); ?> พ.ศ. <?= date('Y') + 543; ?> เวลา ___ : ___ น.</td>
  </tr>
</table>
<hr><br>
<b>หมายเหตุ</b> ให้ชี้แจงทุกครั้งก่อนลงลายมือชื่อ ในหนังสืออนุญาตให้ทำการรักษา / ผู้ให้คำยินยอมได้แก่ ผู้บรรลุนิติภาวะ และมีสติสัมปชัญญะสมบูรณ์ แต่ถ้าผู้ป่วยยังไม่บรรลุนิติภาวะ,ไม่รู้สึกตัว,มีความบกพนร่องทางกายหรือจิต ให้ บิดา มารดา สามี ภรรยา บุตร ญาติใกล้ชิด หรือผู้ปกครองตามกฎหมายลงนาม ยกเว้นกรณีผู้ป่วยสมรสแล้วและมีอายุมากกว่า 17 ปีบริบูรณ์ (มีทะเบียนสมสรตามกฏหมาย) สามารถลงนามเองได้
<pagebreak />
<?php foreach ($doctor_order as $d) { ?>
  <h2 align="center">โรงพยาบาล<?= $hosp->hi_hsp_nm ?></h2>
  <h2 align="center">Doctor Orders Sheet & Progress Note</h2>
  <table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr>
      <td width="45%">
        <font size=6><b>&emsp; ชื่อ-สกุล </b><br>&emsp; &emsp; &emsp; <?= $model->prename . $model->ptname; ?></font>
      </td>
      <td width="17%">
        <font size=6>&emsp; <b>อายุ</b> <?= $model->age; ?> ปี</font>
      </td>
      <td width="38%" v>
        <font size=6>&emsp; <b>HN:</b> <?= $model->hn; ?><br>&emsp; <b>AN:</b> <?= $model->an; ?></font>
      </td>
    </tr>
  </table>
  <table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr>
      <th width="24%">Progress Note</th>
      <th width="10%">Date/Time</th>
      <th width="28%">One Day Order</th>
      <th width="10%">Date/Time</th>
      <th width="28%">Continue Order</th>
    </tr>
    <?php
    if ($model->lcno) {
      $lc = 'ว.' . $model->lcno;
    } else {
      $lc = 'ว. .....................';
    }
    echo "<tr><td height=750 valign=top><font size=5>" . str_replace(array("\n", "\r"), '<br>', $d->progressnote) . "<br><br> " . $model->doctor . "<br>" . $lc . "<br>";
    if ($allergy) {
      echo "<br><b>แพ้ยา </b>" . $allergy->drug . "<br>";
    }
    echo "<br>Past History:<br>" . $model->ph . "</font></td>";
    echo "<td valign=top>" . $d->admit_date . "<br>" . $d->admit_time . " น.</td>";
    echo "<td valign=top><font size=5>";
    $ext = OrderExten::find()->where(['prscno' => $d->prscno])->One();
    echo str_replace(array("\n", "\r"), '<br>', $d->oneday) . "<br>";
    $oneday = OrderOneday::find()->where(['prscno' => $d->prscno])->all();
    $i = 1;
    foreach ($oneday as $od) {
      echo "<p>" . $i . "." . $od->namedrug . "<br>&ensp; - " . $od->meduse . "</p><br>";
      $i = $i + 1;
    }
    echo "<br>" . $ext->labs . "<br>" . $ext->xrays . "<br>" . $ext->proc . "<br>" . "</font></td>";
    echo "<td valign=top>" . $d->admit_date . "<br>" . $d->admit_time . " น.</td>";
    echo "<td valign=top><font size=5>";
    echo str_replace(array("\n", "\r"), '<br>', $d->con) . "<br>";
    $con = OrderCon::find()->where(['prscno' => $d->prscno])->all();
    $i = 1;
    foreach ($con as $c) {
      echo "<p>" . $i . "." . $c->namedrug . "<br>&ensp; - " . $c->meduse . "</p><br>";
      $i = $i + 1;
    }
    echo "</font></td></tr>";
    ?>
  </table>
  <hr>
  <table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr>
      <td valign="top"><b>หมายเหตุ</b></td>
      <td>
        1.ให้มีการบันทึก Progress Note ใน 3 วันแรกของการรักษานรูปแบบ SOAP หากไม่เขียน SOAP กำกับ ขอให้บันทึกในรูปแบบ SOAP<br>
        2.ให้มีการบันทึกทุกครั้งที่มีการเปลี่ยนแปลงแพทย์ที่ดูแล การรักษา การให้ยา การทำ Invasive Procedure และการประเมินผลและแปรผล<br>
        3.บันทึกที่ลายมือที่อ่านออกได้ง่าย และเซ็นชื่อกำกับทุกครั้งโดยให้สามารถระบุได้ว่าเป็นแพทย์ท่านใด<br>
        4.พยาบาลลงลายมือชื่อและตำแหน่งที่สามารถอ่านออกได้ทุกครั้งขณะลงนามในแบบฟอร์มการตรวจของแพทย์พร้อมระบุนเวลาที่รับคำสั่ง
      </td>
    </tr>
  </table>
<?php }
if ($nr) {
?>

  <pagebreak />
  <h2 align="center">แบบบันทึกทางการพยาบาล (Nurse Note)</h2>
  <table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr>
      <th width='10%'>วันที่<br> เวลา</th>
      <th width='5%'>T</th>
      <th width='5%'>P</th>
      <th width='5%'>R</th>
      <th width='10%'>BP</th>
      <th width='25%'>ปัญหา</th>
      <th width='25%'>การพยาบาล</th>
      <th width='15%'>ผู้บันทึก</th>
    </tr>
    <?php foreach ($nr as $n) { ?>
      <tr>
        <td valign="top"><?= $n->note_date; ?><br><br>เวลา&emsp; &emsp; น.</td>
        <td valign="top" align="center">
          <font size=5><?= $n->temp; ?></font>
        </td>
        <td valign="top" align="center">
          <font size=5><?= $n->pluse ?></font>
        </td>
        <td valign="top" align="center">
          <font size=5><?= $n->rr ?></font>
        </td>
        <td valign="top" align="center">
          <font size=5><?= $n->bp; ?></font>
        </td>
        <td valign="top">
          <font size=5><?= str_replace(array("\n", "\r"), '<br>', $n->prob); ?></font>
        </td>
        <td valign="top">
          <font size=5><?= str_replace(array("\n", "\r"), '<br>', $n->imp); ?></font>
        </td>
        <td valign="top">
          <font size=5><?= str_replace(array("\n", "\r"), '<br>', $n->sig); ?></font>
        </td>
      </tr>
    <?php } ?>
    <tr>
      <td colspan="4">
        <font size=6>&emsp; <b>HN:</b> <?= $model->hn; ?><br>&emsp; <b>AN:</b> <?= $model->an; ?></font>
      </td>
      <td colspan="4">
        <font size=6><b>&emsp; ชื่อ-สกุล </b><br>&emsp; &emsp; &emsp; <?= $model->prename . $model->ptname; ?> &emsp; <b>อายุ</b> <?= $model->age; ?> ปี</font>
      </td>
    </tr>
    <tr>
      <td colspan="6">
        <font size=6>&emsp; <b>ตึก</b>ผู้ป่วยใน &emsp; &emsp; <b>เตียง</b> _______</font>
      </td>
      <td colspan="2">
        <font size=6><br>&emsp; <b>แพทย์</b>
          <?php if ($model->doctor) {
            echo ' ' . $model->doctor . '';
          } else {
            echo ' ';
          } ?><br>
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; <?php if ($model->lcno) {
                                                              echo 'ว.' . $model->lcno;
                                                            } ?>
        </font>
      </td>
    </tr>
  </table>
<?php } ?>