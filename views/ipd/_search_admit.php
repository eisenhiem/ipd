<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IptSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ipt-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
<div class="row">
<div class="col-md-3">
<?= $form->field($model, 'hn') ?>
</div>
<div class="col-md-3">
<?= $form->field($model, 'an') ?>
</div>
</div>


    <div class="form-group">
        <?= Html::submitButton('ค้นหา', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
