<?php

use miloschuman\highcharts\Highcharts;
use miloschuman\highcharts\SeriesDataHelper;
use yii\grid\GridView;
use yii\helpers\Html;

function egfr($age, $creat, $sex)
{
    if ($creat) {
        if ($sex == 'ชาย') {
            if ($creat > 0.9) {
                $egfr = 141  * pow(0.993, $age) / pow(($creat / 0.9), 1.209);
            } else {
                $egfr = 141  * pow(0.993, $age) / pow(($creat / 0.9), 0.411);
            }
        } else {
            if ($creat > 0.9) {
                $egfr = 144  * pow(0.993, $age) / pow(($creat / 0.7), 1.209);
            } else {
                $egfr = 144  * pow(0.993, $age) / pow(($creat / 0.7), 0.329);
            }
        }
    } else {
        $egfr = 'NA';
    }
    return round($egfr, 2);
}

function getThaiDateShort($date)
{
    $month_th = array("01" => "ม.ค.", "02" => "ก.พ.", "03" => "มี.ค.", "04" => "เม.ย.", "05" => "พ.ค.", "06" => "มิ.ย.", "07" => "ก.ค.", "08" => "ส.ค.", "09" => "ก.ย.", "10" => "ต.ค.", "11" => "พ.ย.", "12" => "ธ.ค.");
    if ($date) {
        $v = new DateTime($date);
        $d = $v->format('j');
        $m = $month_th[$v->format('m')];
        $y = $v->format('Y') + 543;
        $result = $d . ' ' . $m . ' ' . $y;
    } else {
        $result = 'ไม่ได้ระบุวัน';
    }
    return $result;
}

?>
<table calss="table_bordered" width="100%" border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td width="30%">
            <h2>MINISTORY OF HEALTH<br>&emsp;&emsp;&emsp;THAILAND</h2>
        </td>
        <td width="40%">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<img src="../web/imgs/crut.png" width="80"></td>
        <td width="30%">
            <h2>LAOSUEAKOK HOSPITAL<br>IN-PATIENT SUMMARY</h2>
        </td>
    </tr>
</table>
<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
    <tr>
        <td colspan=8 width="100%">
            <b>สิทธิ์ : </b><?php echo $model->pt_type; ?>
        </td>
    </tr>
    <tr>
        <td colspan=2 width="25%"><b>AN : </b><?php echo $model->an; ?> </td>
        <td colspan=4 width="50%"><b>PERSON IDENIFIED NUMBER </b><?php echo $model->cid; ?></td>
        <td colspan=2 width="25%"><b>HN : </b><?php echo $model->hn; ?></td>
    </tr>
    <tr>
        <td colspan=3 width="37.5%"><b>PATIENT </b><?php echo $model->prename . $model->ptname; ?></td>
        <td width="12.5%"><b>SEX </b><?php echo $model->sex; ?></td>
        <td colspan=2 width="25%"><b>DATE OF BIRTH </b><?php echo getThaiDateShort($model->brthdate); ?></td>
        <td width="12.5%"><b>AGE </b><?php echo $model->age . ' ปี'; ?></td>
        <td width="12.5%"><b>BW </b><?php echo $model->bw . ' kg'; ?></td>
    </tr>
    <tr>
        <td colspan=2 width="25%"><b>NATIONALITY </b><?php echo $model->nation; ?></td>
        <td colspan=2 width="25%"><b>RELEGION </b><?php echo $model->religion; ?></td>
        <td colspan=2 width="25%"><b>OCCUPATION </b><?php echo $model->job; ?></td>
        <td colspan=2 width="25%"><b>MARRY STATUS </b><?php echo $model->married_status; ?></td>
    </tr>
    <tr>
        <td colspan=8>
            <b>ADDRESS </b><?php echo $model->address . ' TELEPHONE ' . $model->hometel; ?>
        </td>
    </tr>
    <tr>
        <td colspan=8>
            <b>PATIENT NOTIFIED</b>
            <?php echo $model->relate_person . ' เกี่ยวข้องเป็น ' . $model->relation . ' ที่ติดต่อ' . $model->relate_addr . ' เบอร์ติดต่อ ' . $model->infmtel; ?>
        </td>
    </tr>
    <tr>
        <td valign="top"><b>GRAVIDITY</b></td>
        <td valign="top"><b>PARITY</b></td>
        <td valign="top"><b>LIVING CHILD</b></td>
        <td colspan=4><b>CONDITION OF CHILD AT BIRTH </b>
            <br>1.LIVEBORN 2.STILLBIRTH 3.CLINICAL MATURE
            <br>4.CLINIC PREMATURE
        </td>
        <td valign="top"><b>BIRTH WEIGTH</b></td>
    </tr>
    <tr>
        <td valign="top"><b>WARD </b><br><?php echo $model->ward; ?><br> <b>BED</b> <br><?php echo $model->bed; ?></td>
        <td valign="top" colspan=2>
            <b>DEPARTMENT</b><br>
            1.MED 2.SUR 3.OBS 5.PED<br>
            10.DENT<br>
            12.OTHER_____________
        </td>
        <td valign="top" colspan=4>
            DATE OF &emsp; &emsp;DAY MONTH YEAR &emsp; &emsp; HOUR:MIN </b><br>
            ADMISSION &emsp;&emsp;&emsp;<?php echo getThaiDateShort($model->rgtdate); ?> &emsp; &emsp;&emsp;&emsp;<?php echo $model->rg_time . ' น.'; ?><br>
            DISCHARGE&emsp;&emsp;&emsp;<?php echo getThaiDateShort($model->dchdate); ?> &emsp; &emsp;&emsp;&emsp;<?php echo $model->dc_time . ' น.'; ?>
        </td>
        <td valign="top"><b>LENGTH OF STAY </b><br> &emsp;<?php echo $model->losd; ?> วัน
        </td>
    </tr>
    <tr>
        <td colspan=4 valign="top"><b>PRINCIPLE DIAGNOSIS</b><br><?php //Echo strtoupper($diag->pdx_name); 
                                                                    ?><br>&emsp;</td>
        <td colspan=2 valign="top" align="center"><b>ICD10</b><br><?php //Echo $diag->pdx_code; 
                                                                    ?></td>
        <td colspan=2 rowspan=2 valign="top"><b>Clinical Summary</b><br><?php if ($ipd_pe) {
                                                                            echo $ipd_pe->problem;
                                                                        } ?></td>
    </tr>
    <tr>
        <td colspan=4 valign="top"><b>COMOBIDITY DIAGNOSIS</b><br><?php //Echo strtoupper($diag->comb_name); 
                                                                    ?><br>&emsp;<br>&emsp;<br>&emsp;</td>
        <td colspan=2 valign="top"><?php //Echo $diag->comb_code; 
                                    ?></td>
    </tr>
    <tr>
        <td colspan=4 valign="top"><b>COMPLICATION DIAGNOSIS</b><br><?php //Echo strtoupper($diag->comp_name); 
                                                                    ?><br>&emsp;<br>&emsp;</td>
        <td colspan=2 valign="top"><?php //Echo $diag->comp_code; 
                                    ?></td>
        <td colspan=2 rowspan=2 valign="top"><b>Important Investigation</b><br></td>
    </tr>
    <tr>
        <td colspan=4 valign="top"><b>OTHER DIAGNOSIS</b><br><?php //Echo strtoupper($diag->other_name); 
                                                                ?><br>&emsp;<br>&emsp;</td>
        <td colspan=2 valign="top"><?php //Echo $diag->other_code; 
                                    ?></td>
    </tr>
    <tr>
        <td colspan=4 valign="top"><b>EXTERNAL CAUSE OF INJURY</b><br><?php //Echo strtoupper($diag->ex_name); 
                                                                        ?><br>&emsp;<br>&emsp;</td>
        <td colspan=2 valign="top"><?php //Echo $diag->ex_code; 
                                    ?></td>
        <td colspan=2 rowspan=2 valign="top"><b>Plan of treatment, Lab Follow up, Appointment</b><br></td>
    </tr>
    <tr>
        <td colspan=6 valign="top"><b>NON OPERATIVE PROCEDURE</b><br>
            &emsp;<br>
            <?php if ($proc) { ?>
                &emsp;(<?php if ($proc->Dressing > 0) {
                            echo ' X ';
                        } else {
                            echo ' ';
                        } ?>) DRESSING
                (<?php if ($proc->ID > 0) {
                        echo ' X ';
                    } else {
                        echo ' ';
                    } ?>) I&D
                (<?php if ($proc->DEBRIDEMENT > 0) {
                        echo ' X ';
                    } else {
                        echo ' ';
                    } ?>) DEBRIDEMENT
                (<?php if ($proc->US > 0) {
                        echo ' X ';
                    } else {
                        echo ' ';
                    } ?>) U/S
                (<?php if ($proc->EKG > 0) {
                        echo ' X ';
                    } else {
                        echo ' ';
                    } ?>) EKG
                (<?php if ($proc->O2 > 0) {
                        echo ' X ';
                    } else {
                        echo ' ';
                    } ?>) OXYGEN TERAPHY < 96 hr. <br>&emsp;<br>
                    &emsp;(<?php if ($proc->Blood > 0) {
                                echo ' X ';
                            } else {
                                echo ' ';
                            } ?>) TRANSFUSION OF PACKED CELL
                    (<?php if ($proc->TUBE > 0) {
                            echo ' X ';
                        } else {
                            echo ' ';
                        } ?>) ET TUBE
                <?php
            } else {
                echo '&emsp;( ) DRESSING ( ) I&D ( ) DEBRIDEMENT ( ) U/S ( ) EKG ( ) OXYGEN TERAPHY < 96 hr. <br> &emsp;( ) TRANSFUSION OF PACKED CELL ( ) ET TUBE ';
            }
                ?>

                (&emsp;) ..........................................................
        </td>
    </tr>
    <tr>
        <td colspan=6 valign="top"><b>OPERATIVE PROCEDURE</b> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; BY &emsp; &emsp; &emsp;&emsp; DATE &emsp;&emsp; TIME IN &emsp;&emsp; TIME OUT
            <br>&emsp;<br>
            &emsp;1) _____________________________&emsp;____________&emsp;___/___/____&emsp;___ : ___ น.&emsp;___ : ___ น. <br>&emsp;<br>
            &emsp;2) _____________________________&emsp;____________&emsp;___/___/____&emsp;___ : ___ น.&emsp;___ : ___ น. <br>
        </td>
        <td colspan=2 valign="top"><b>PROCEDURE CODE</b></td>
    </tr>
    <tr>
        <td colspan=3 valign="top"><b>DISCHARGE STATUS </b><br>
            &emsp;[<?php if ($model->dc_status == 1) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 1.COMPLETE RECOVERY <br>
            &emsp;[<?php if ($model->dc_status == 2) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 2.IMPROVE <br>
            &emsp;[<?php if ($model->dc_status == 3) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 3.NOT IMPROVE <br>
            &emsp;[<?php if ($model->dc_status == 4) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 4.NORMAL DELIVERY <br>
            &emsp;[<?php if ($model->dc_status == 5) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 5.UNDELEVERY <br>
            &emsp;[<?php if ($model->dc_status == 6) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 6.NORMAL CHILD D/C WITH MOTHER <br>
            &emsp;[<?php if ($model->dc_status == 7) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 7.NORMAL CHILD D/C SEPARATERY <br>
            &emsp;[<?php if ($model->dc_status == 8) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 8.STILL BIRTH <br>
            &emsp;[<?php if ($model->dc_status == 9) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 9.DEATH
        </td>
        <td colspan=3 valign="top"><b>DISCHARGE TYPE </b><br>
            &emsp;[<?php if ($model->dc_type == 1) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 1.WITH IMPROVE <br>
            &emsp;[<?php if ($model->dc_type == 2) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 2.AGAINT IMPROVE <br>
            &emsp;[<?php if ($model->dc_type == 3) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 3.ESCAPE <br>
            &emsp;[<?php if ($model->dc_type == 4) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 4.TRANSFER <br>
            &emsp; REFER TO <?php echo $model->refer_hosp; ?><br>
            &emsp; CAUSE <?php echo $model->refer_cause; ?><br>
            &emsp;[<?php if ($model->dc_type == 5) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 5.OTHER <br>
            &emsp;[<?php if ($model->dc_type == 8) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 8.DEAD AUTORY <br>
            &emsp;[<?php if ($model->dc_type == 9) {
                        echo ' X ';
                    } else {
                        echo '&emsp; ';
                    } ?>] 9.DEAD NO AOTORY <br>
        </td>
        <td colspan=2 valign="top"><b>DIAGNOSIS CODE BY </b><br>
            &emsp;<br>
            &emsp;<br>
            &emsp;<br>
            &emsp;<br>
            <b>PROCEDURE CODE BY</b>
        </td>
    </tr>
    <tr>
        <td width="12.5%">&emsp;[<?php if ($model->follow_up) {
                                        echo ' X ';
                                    } else {
                                        echo '&emsp; ';
                                    } ?>] F/U <?php echo substr($model->follow_up, 0, 10); ?><br>&emsp;[<?php if ($model->follow_up) {
                                                                                                            echo '  ';
                                                                                                        } else {
                                                                                                            echo ' X ';
                                                                                                        } ?>] เก็บ</td>
        <td width="12.5%">สรุปค่าใช้จ่าย<br>&emsp;</td>
        <td width="12.5%">รับชาร์จ<br>&emsp;</td>
        <td width="12.5%">ส่งแพทย์สรุป<br>&emsp;</td>
        <td width="12.5%">รับจากแพทย์<br>&emsp;</td>
        <td width="12.5%">Audit Code<br>&emsp;</td>
        <td width="12.5%">ส่งเคลม<br>&emsp;</td>
        <td width="12.5%">AdjRW<br>&emsp;</td>
    </tr>
</table>
<br>
<table calss="table_bordered" width="100%" border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td width="50%">
            <h3>ATTENSING PHYSICIAN</h3>
        </td>
        <td width="50%">
            <h3>APPROVE BY </h3>
        </td>
    </tr>
    <tr>
        <td width="50%">&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
            &emsp;<?php if ($model->doctor) {
                        echo $model->doctor . ' ว.' . $model->lcno;
                    } ?></td>
        <td width="50%">&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; นพ.ณัฐวุฒิ พูลเขาล้าน ว.54376 </td>
    </tr>
</table>
<?php if ($dataProvider) {
?>
    <pagebreak />

    <hr>
    <b>AN : </b><?php echo $model->an; ?>
    <b>HN : </b><?php echo $model->hn; ?>
    <b>ชื่อ-สกุล : </b><?php echo $model->prename . $model->ptname; ?>
    <b>เพศ : </b><?php echo $model->sex; ?>
    <b>อายุ :</b><?php echo $model->age . ' ปี'; ?>

    <hr>
    <!-- 
<b>Important Investigation</b>
<hr>
<table calss="table_bordered" width="100%" border="0" cellpadding="2" cellspacing="0">
    <tr>
        <?php if ($fcbc->wbc) { ?>
        <td width="25%" valign="top"
            style="background-image:url(../web/imgs/cbc2.jpg);background-repeat:no-repeat;background-size:160px 100px;   width: 160px; height: 100px;">
            &emsp; <?php echo $fcbc->hb; ?>&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; <?php echo $fcbc->pmn; ?>
            <br> &emsp; &emsp; &emsp; &emsp;&emsp;<?php echo $fcbc->wbc; ?> <br>&emsp;
            <br> &emsp; &emsp; &emsp; &emsp; <?php echo $fcbc->pltc; ?>
            <br> &emsp; <?php echo $fcbc->hct; ?>&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
            <?php echo $fcbc->lym; ?>
            <br> &emsp; <?php if ($lablist) {
                            echo 'date : ' . $lablist->fdate_cbc;
                        } ?>
        </td>
        <?php } ?>
        <?php if ($lcbc->wbc) { ?>
        <td width="25%" valign="top"
            style="background-image:url(../web/imgs/cbc2.jpg);background-repeat:no-repeat;background-size:160px 100px;   width: 160px; height: 100px;">
            &emsp; <?php echo $lcbc->hb; ?>&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; <?php echo $lcbc->pmn; ?>
            <br> &emsp; &emsp; &emsp; &emsp;&emsp;<?php echo $lcbc->wbc; ?> <br>&emsp;
            <br> &emsp; &emsp; &emsp; &emsp; <?php echo $lcbc->pltc; ?>
            <br> &emsp; <?php echo $lcbc->hct; ?>&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
            <?php echo $lcbc->lym; ?>
            <br> &emsp; <?php if ($lablist) {
                            echo 'date : ' . $lablist->ldate_cbc;
                        } ?>
        </td>
        <?php } ?>
        <?php if (false) { ?>
        <td width="25%" valign="top"
            style="background-image:url(../web/imgs/elyte2.jpg);background-repeat:no-repeat;background-size:160px 100px;   width: 160px; height: 100px;">
            <br> &emsp;&emsp; &emsp;
            <?php echo $felyte->na; ?>&emsp; &emsp; <?php echo $felyte->k; ?>
            <br> &emsp;<br>&emsp;&emsp; &emsp;
            <?php echo $felyte->cl; ?>&emsp; &emsp; <?php echo $felyte->co2; ?>
            <br><br> &emsp; <?php if ($lablist) {
                                echo 'date : ' . $lablist->fdate_elyte;
                            } ?>
        </td>
        <?php } ?>
        <?php if (false) { ?>
        <td width="25%" valign="top"
            style="background-image:url(../web/imgs/elyte2.jpg);background-repeat:no-repeat;background-size:160px 100px;   width: 160px; height: 100px;">
            <br> &emsp;&emsp; &emsp;
            <?php echo $lelyte->na; ?>&emsp; &emsp; <?php echo $lelyte->k; ?>
            <br> &emsp;<br>&emsp;&emsp; &emsp;
            <?php echo $lelyte->cl; ?>&emsp; &emsp; <?php echo $lelyte->co2; ?>
            <br><br> &emsp; <?php if ($lablist) {
                                echo 'date : ' . $lablist->ldate_elyte;
                            } ?>
        </td>
        <?php } ?>
    </tr>
</table>
<?php
    if ($fcreat->labresult) {
        echo 'Creatinine: ' . $fcreat->labresult . ' mg/dL ';
        echo 'eGFR(CKD-EPI): ' . egfr($model->age, $fcreat->labresult, $model->sex) . ' mL/min/1.73^2 ';
        if ($lablist) {
            echo 'Date : ' . $lablist->fdate_creat;
        }
    }
?>
<br>
<?php
    if ($lcreat->labresult) {
        echo 'Creatinine: ' . $lcreat->labresult . ' mg/dL ';
        echo 'eGFR(CKD-EPI): ' . egfr($model->age, $lcreat->labresult, $model->sex) . ' mL/min/1.73^2 ';
        if ($lablist) {
            echo 'Date : ' . $lablist->ldate_creat;
        }
    }
?>
<br>
<br>
<br>
!-->
    <b>Home Medicine</b>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'dname',
                'contentOptions' => ['style' => 'width: 300px'],
            ],
            [
                'attribute' => 'medusage',
                'value' => function($model){
                    return $model->getMeduse();
                }
            ],
            [
                'attribute' => 'qty',
                'contentOptions' => ['style' => 'width: 50px;text-align: center;'],
            ],
            [
                'attribute' => 'unit',
                'contentOptions' => ['style' => 'width: 50px;text-align: center;'],
            ],
        ],
    ]);
    ?>
    <hr>
<?php } ?>