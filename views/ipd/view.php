<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ListIpd */

$this->title = $model->an;
$this->params['breadcrumbs'][] = ['label' => 'List Ipds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="list-ipd-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Print', ['print', 'id' => $model->an], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Print Summary', ['dcprint', 'id' => $model->an], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'an',
            'hn',
            'prename',
            'ptname',
            'age',
            'cc',
            'pi:ntext',
            'ph:ntext',
            'pe:ntext',
            'bw',
            'tt',
            'sbp',
            'dbp',
            'pr',
            'rr',
            'height',
            'bmi',
            'prediag:ntext',
            'doctor',
            'lcno',
        ],
    ]) ?>

</div>
