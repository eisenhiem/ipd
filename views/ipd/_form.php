<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ipt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ipt-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vn')->textInput() ?>

    <?= $form->field($model, 'an')->textInput() ?>

    <?= $form->field($model, 'hn')->textInput() ?>

    <?= $form->field($model, 'rgtdate')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
