<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IptSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ทะเบียนผู้ป่วยรับรักษา';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ipt-index">

    <?php echo $this->render('_search_admit', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'vn',
            'an',
            'hn',
            'cid',
            'ptname',
            //'rgtdate',
            //'rgttime:datetime',
            //'pttype',
            //'prediag',
            //'dchdate',
            //'daycnt',
            //'dchtime:datetime',
            //'dchstts',
            //'dchtype',
            //'dthdiagdct',
            //'ipdlct',
            //'bmi',
            //'bw',
            //'height',
            //'tt',
            //'rr',
            //'pr',
            //'sbp',
            //'dbp',
            //'ipttype',
            //'painscore',
            //'symptmi',
            //'drg',
            //'rw',
            //'adjrw',
            //'error',
            //'warning',
            //'actlos',
            //'grouper_version',
            //'wtlos',
            //'ot',

            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{print}',
                'buttons'=>[
                    'print' => function($url,$model,$key){
                      return Html::a('Print',['ipd/print','id'=>$model->an],['class' => 'btn btn-primary','target'=>'_blank']);
                    }
                ]
             ],
        ],
    ]); ?>


</div>
