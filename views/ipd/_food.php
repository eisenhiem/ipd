<?php 
use app\models\Calendar;

$d = Calendar::find()->all();

foreach ($d as $i){
?>

<h2 align="center">ใบเบิกอาหารตามตึกผู้ป่วย</h2>
<h3 align="center">ฝ่ายโภชนาการโรงพยาบาลเหล่าเสือโก้ก ตึกผู้ป่วยใน 
    <br>วันที่ <?= $i->getDateorder() ?> มิถุนายน พ.ศ. 2564 เวลา ........... น.
</h3>

<table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
<tr>
<th>เตียง</th>
<th>ชื่อ-สกุล</th>
<th>อายุ</th>
<th>อาหารหลัก</th>
<th>อาหารรอง</th>
<th>FEED</th>
<th>CC</th>
<th>ไข่ขาว/มื้อ</th>
<th>หมายเหตุ</th>
</tr>
<?php 
 foreach ($model as $data) {
     if($data->calendar_date == $i->calendar_date){
        echo "<tr><td>".$data->bed."</td><td>".$data->ptinfo->prename.$data->ptinfo->ptname."</td><td>".$data->ptinfo->age."</td><td>REGULAR DIET</td><td></td><td></td><td></td><td></td><td></td></tr>";
     }
 }
?>
</table>

<sethtmlpagefooter name="sum" value="on"/>
<htmlpagefooter name="sum">
<table>
    <tr>
    <td width="44%">
    <table calss="table_bordered" width="100%" border="1" cellpadding="2" cellspacing="0">
        <tr>
            <th>ชนิดอาหาร</th>
            <th>พิเศษ</th>
            <th>50/ที่</th>
            <th>สามัญ</th>
            <th>31/ที่</th>
        </tr>
        <tr>
            <td>อาหารธรรมดา</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>อาหารเฉพาะโรค</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>อาหารเหลวทั่วไป</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>อาหารปั่นทางสายยาง</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>อาหารอื่น</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>รวม</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</td>
<td>
    ผู้สั่งอาหาร &emsp;&emsp;&emsp;&emsp;&emsp;&emsp; (หัวหน้าเวรหรือผู้ด้รับมอบหมาย) <br>
    <br>
    ผู้รับทำอาหาร
    <br>&emsp;&emsp;&emsp;&emsp;(นายชาญณรงค์  เหง้าเกษ)<br><br>
    กำหนดส่งมอบอาหารภายในเวลา &emsp;&emsp;&emsp;&emsp; น. ของวันที่ <?= $i->getDateorder() ?>
    <br>
    และผู้ตรวจรับได้ทำการตรวจสอบครบถ้วนถูกต้องแล้วจึงได้ลงชื่อไว้เป็นหลักฐาน<br>
    <br>
    ผู้ตรวจรับอาหาร&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;หัวหน้าเวร<br>    
</td>
</tr>
</table>
<br>
เรียนผู้ว่าราชการจังหวัดอุบลราชธานี<br>
&emsp;เพื่อทราบ&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;เจ้าหน้าที่<br>
&emsp;&emsp;&emsp;(&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;)&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;ทราบ<br>
<br>
&emsp;ทราบ&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;หัวหน้าเจ้าหน้าที่<br>
&emsp;&emsp;&emsp;(นางพัชรา  เดชาวัตร)
</htmlpagefooter>
<pagebreak>

</pagebreak>
<?php
}
?>
