<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ovst */

$this->title = 'Update Ovst: ' . $model->vn;
$this->params['breadcrumbs'][] = ['label' => 'Ovsts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->vn, 'url' => ['view', 'id' => $model->vn]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ovst-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
