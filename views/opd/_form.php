<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ovst */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ovst-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'vstdttm')->textInput() ?>

    <?= $form->field($model, 'hn')->textInput() ?>

    <?= $form->field($model, 'cln')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'dct')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pttype')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sickdate')->textInput() ?>

    <?= $form->field($model, 'ovstist')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ovstost')->textInput() ?>

    <?= $form->field($model, 'bw')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'height')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pr')->textInput() ?>

    <?= $form->field($model, 'rr')->textInput() ?>

    <?= $form->field($model, 'sbp')->textInput() ?>

    <?= $form->field($model, 'dbp')->textInput() ?>

    <?= $form->field($model, 'an')->textInput() ?>

    <?= $form->field($model, 'register')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'waist_cm')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
