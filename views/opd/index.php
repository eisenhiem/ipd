<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OvstSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ovsts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ovst-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= Html::a('พิมพ์ ใบนำทาง Vaccine', ['vacc'], ['class' => 'btn btn-primary']) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'vstdttm',
            'hn',
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{print}',
                'buttons'=>[
                    'print' => function($url,$model,$key){
                      return Html::a('Print',['opd/print','id'=>$model->vn],['class' => 'btn btn-primary','target'=>'_blank']);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{ncd}',
                'buttons'=>[
                    'ncd' => function($url,$model,$key){
                      return Html::a('ใบแนบสมุด',['opd/ncd','id'=>$model->vn],['class' => 'btn btn-primary','target'=>'_blank']);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:120px;'],
                'buttonOptions'=>['class'=>'btn btn-primary'],
                'template'=>'{rep}',
                'buttons'=>[
                    'rep' => function($url,$model,$key){
                        $pttype = ['10','11','12','13','14','21','22','23','37','70'];
                        if(in_array($model->pttype,$pttype)){
                            return Html::a('ใบทวงหนี้',['opd/rep','id'=>$model->vn],['class' => 'btn btn-warning','target'=>'_blank']);
                        }
                    }
                ]
            ],
        ],

    ]); ?>


</div>
