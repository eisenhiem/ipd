<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ovst */

$this->title = 'Create Ovst';
$this->params['breadcrumbs'][] = ['label' => 'Ovsts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ovst-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
