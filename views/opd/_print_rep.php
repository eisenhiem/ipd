<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$birthdate = new DateTime($ptinfo->birth);
$visit = new DateTime($model->vstdttm);
$visit_date = date_format($visit, 'd/m/Y');
$visit_time = date_format($visit, 'H:i');

$pdate = new DateTime();
$pday = date_format($pdate, 'j');
$pmonth = date_format($pdate, 'm');
$pyear = date_format($pdate, 'Y') + 543;
$thaiweek = array("วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัส", "วันศุกร์", "วันเสาร์");
$thaimonth = array("มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
//echo $thaiweek[date("w")] ,"ที่",date(" j "), $thaimonth[date(" m ")-1] , " พ.ศ. ",date(" Y ")+543;

function num2wordsThai($num)
{
  $num = str_replace(",", "", $num);
  $num_decimal = explode(".", $num);
  $num = $num_decimal[0];
  $returnNumWord = '';
  $lenNumber = strlen($num);
  $lenNumber2 = $lenNumber - 1;
  $kaGroup = array("", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");
  $kaDigit = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ต", "แปด", "เก้า");
  $kaDigitDecimal = array("ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ต", "แปด", "เก้า");
  $ii = 0;
  for ($i = $lenNumber2; $i >= 0; $i--) {
    $kaNumWord[$i] = substr($num, $ii, 1);
    $ii++;
  }
  $ii = 0;
  for ($i = $lenNumber2; $i >= 0; $i--) {
    if (($kaNumWord[$i] == 2 && $i == 1) || ($kaNumWord[$i] == 2 && $i == 7)) {
      $kaDigit[$kaNumWord[$i]] = "ยี่";
    } else {
      if ($kaNumWord[$i] == 2) {
        $kaDigit[$kaNumWord[$i]] = "สอง";
      }
      if (($kaNumWord[$i] == 1 && $i <= 2 && $i == 0) || ($kaNumWord[$i] == 1 && $lenNumber > 6 && $i == 6)) {
        if ($kaNumWord[$i + 1] == 0) {
          $kaDigit[$kaNumWord[$i]] = "หนึ่ง";
        } else {
          $kaDigit[$kaNumWord[$i]] = "เอ็ด";
        }
      } elseif (($kaNumWord[$i] == 1 && $i <= 2 && $i == 1) || ($kaNumWord[$i] == 1 && $lenNumber > 6 && $i == 7)) {
        $kaDigit[$kaNumWord[$i]] = "";
      } else {
        if ($kaNumWord[$i] == 1) {
          $kaDigit[$kaNumWord[$i]] = "หนึ่ง";
        }
      }
    }
    if ($kaNumWord[$i] == 0) {
      if ($i != 6) {
        $kaGroup[$i] = "";
      }
    }
    $kaNumWord[$i] = substr($num, $ii, 1);
    $ii++;
    $returnNumWord .= $kaDigit[$kaNumWord[$i]] . $kaGroup[$i];
  }
  if (isset($num_decimal[1])) {
    $returnNumWord .= "บาท";
    if ($num_decimal[1] = "00") {
      $returnNumWord .= "ถ้วน";
    } else {
      for ($i = 0; $i < strlen($num_decimal[1]); $i++) {
        $returnNumWord .= $kaDigitDecimal[substr($num_decimal[1], $i, 1)];
      }
      $returnNumWord .= "สตางค์";
    }
  }
  return $returnNumWord;
}

function age($bdate, $vdate)
{
  $difference = $bdate->diff($vdate);
  $age = $difference->format('%y');
  return $age;
}


?>
<div class="container" style="padding-left:1cm;padding-right:0.5cm;padding-top:0.5cm;">
  <table style="width:100%">
    <tr>
      <td style="vertical-align:bottom;font-size:1.3rem;width:250px"> ที่ อบ 0032.301.009 / ..........</td>
      <td style="width:100px"><img src="../web/imgs/crut.png" width="80"></td>
      <td style="vertical-align:bottom;font-size:1.3rem;">โรงพยาบาลเหล่าเสือโก้ก</td>
    </tr>
    <tr>
      <td rowspan="2" colspan="2">&emsp;</td>
      <td style="font-size:1.3rem;">อำเภอเหล่าเสือโก้ก</td>
    </tr>
    <tr>
      <td style="font-size:1.3rem;">จังหวัดอุบลราชธานี 34000</td>
    </tr>
  </table>
  <p style="font-size:1.3rem;">
    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; &emsp;วันที่ <?= $pday . ' ' . $thaimonth[date(" m ") - 1] ?> พ.ศ. <?= $pyear ?><br>
    เรื่อง ขอให้ชำระค่ารักษาพยาบาล ค้างจ่าย <br>
    เรียน <?php echo 'คุณ' . $ptinfo->ptname; ?>
  </p>
  <p style="font-size:1.3rem;">
    &emsp;&emsp;&emsp;&emsp;ตามที่ <?php echo $ptinfo->prename . $ptinfo->ptname; ?>
    HN <?php echo $model->hn; ?> ที่อยู่ <?php echo $ptinfo->address; ?>
    ได้เข้ารักษาพยาบาลที่โรงพยาบาลเหล่าเสือโก้ก เนื่องจากการเจ็บป่วย
    เมื่อวันที่ <?php echo date_format($visit, 'j') . ' ' . $thaimonth[date_format($visit, " m ") - 1]; ?> พ.ศ. <?php echo date_format($visit, 'Y') + 543; ?>
    ด้วยสิทธิการรักษา <?php echo $pttype->namepttype; ?>
    นั้นได้มีการค้างค่ารักษาพยาบาล เป็นจำนวนเงิน <?= $total ?> บาท (<?= num2wordsThai($total) ?>)
    จึงขอให้ท่านมาติดต่องานประกันสุขภาพโรงพยาบาลเหล่าเสือโก้กเพื่อชำระค่ารักษาพยาบาลดังกล่าว ในวันราชการ ภายในวันที่
    <?= $pday ?>
    <?php if (date(" m ") <> 12) {
      echo $thaimonth[number_format(date(" m "))];
    } else {
      echo $thaimonth[0];
    } ?> พ.ศ. <?php if (date(" m ") <> '12') {
                echo $pyear;
              } else {
                echo $pyear + 1;
              } ?> เวลา 08.00 น. – 16.00 น. และขออภัยหากท่านได้ชำระยอดค้างชำระเรียบร้อยแล้ว<br>
    &emsp;&emsp;&emsp;&emsp;จึงเรียนมาเพื่อโปรดทราบและกรุณาเดินทางไปชำระค่ารักษาพยาบาลค้างจ่ายด้วย
  </p>
  <br>
  <br>
  <br>
  <br>
  <p style="font-size:1.3rem;">
    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;(นางสุรภารัชต์ ทองหิน)<br>
    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; &emsp; &emsp;พยาบาลวิชาชีพชำนาญการ<br>
    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;หัวหน้ากลุ่มงานประกันสุขภาพโรงพยาบาลเหล่าเสือโก้ก<br>
    <br>
    <br>
    <br>
    <br>
    <br>
    งานประกันสุขภาพโรงพยาบาลเหล่าเสือโก้ก <br>
    โทร. 045-304205-6 ต่อ 124<br>
    ผู้ประสานงาน นางสุรภารัชต์ ทองหิน 088-5669145<br>
  </p>
</div>