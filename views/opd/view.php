<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Ovst */

$this->title = $model->vn;
$this->params['breadcrumbs'][] = ['label' => 'Ovsts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ovst-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->vn], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->vn], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'vn',
            'vstdttm',
            'hn',
            'nrxtime:datetime',
            'drxtime:datetime',
            'cln',
            'dct',
            'pttype',
            'sickdate',
            'ovstist',
            'ovstost',
            'overtime:datetime',
            'bw',
            'height',
            'bmi',
            'tt',
            'pr',
            'rr',
            'sbp',
            'dbp',
            'preg',
            'tb',
            'toq',
            'drink',
            'mr',
            'an',
            'rcptno',
            'register',
            'smoke',
            'waist_cm',
        ],
    ]) ?>

</div>
