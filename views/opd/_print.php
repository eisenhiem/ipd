<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Meditem;
use app\models\Lab;
use app\models\Xray;
use app\models\Clinic;
use app\models\Hospcode;
use app\models\Dct;
use app\models\Lcbc;
use app\models\Lelyte;
use app\models\LCr;
use app\models\Ldtx;
use app\models\Medusage;

$birthdate = new DateTime($ptinfo->birth);
$visit = new DateTime($model->vstdttm);
$visit_date = date_format($visit,'d/m/Y');
$visit_time = date_format($visit,'H:i');

function age($bdate,$vdate){
  $difference = $bdate->diff($vdate); 
  
  $age = $difference->format('%y');  
  return $age;
} 

function egfr($age,$creat,$sex){
  if ($creat){
    if($sex == '1'){
      if($creat > 0.9) {
        $egfr = 141  * pow(0.993,$age) / pow(($creat/0.9),1.209);
      } else {
        $egfr = 141  * pow(0.993,$age) / pow(($creat/0.9),0.411);
      }
    } else {
      if($creat > 0.9) {
        $egfr = 144  * pow(0.993,$age) / pow(($creat/0.7),1.209);
      } else {
        $egfr = 144  * pow(0.993,$age) / pow(($creat/0.7),0.329);
      }
    }  
  } else {
    $egfr = 'NA';
  }
  return round($egfr,2);
}

?>
  <h2 align="center">ประวัติการรักษาผู้ป่วยนอก OPD CARD</h2>
  <h3 align="center">โรงพยาบาลเหล่าเสือโก้ก อำเภอเหล่าเสือโก้ก จังหวัดอุบลราชธานี</h3>
<p>
   ชื่อ : <?php echo $ptinfo->prename.$ptinfo->ptname; ?> เพศ :<?php echo $ptinfo->sex;?> 
   อายุ : <?php echo age($birthdate,$visit); ?> ปี HN :<?php echo $model->hn; ?> เลขบัตรประชาชน :<?php echo $ptinfo->cid;?>
   สิทธิ์ : <?php echo $pttype->namepttype; ?><br>
   รับบริการวันที่ <?php echo $visit_date; ?> เวลา <?php echo $visit_time; ?> น. 
   แผนก <?php echo $clinic->namecln; ?>
</p>
<p><b>Vitaul Sign</b>
  BT : <?php echo $model->tt; ?> ºC, BP : <?php echo $model->sbp.'/'.$model->dbp; ?> mmHg, PR : <?php echo $model->pr?> /m, RR <?php echo $model->rr?> /m,
  BW : <?php echo $model->bw; ?> kg, ส่วนสูง :<?php echo $model->height?> cm, BMI :<?php if($model->height <> 0) { echo round(($model->bw)/(($model->height/100)*($model->height/100)),2);} ?> 
</p>
<p>
<table>
  <tr>
    <td width=40%  valign="top">
      <b>Chief Compaint :</b> <?php echo $cc->symptom; ?>
    </td>
    <td rowspan="2" colspan="4"  valign="top">
      <b>Diagnosis :</b> <br>
      <?php 
        foreach($diag as $dx){
          echo $dx->icd10.'=>'.$dx->icd10name."<br>";
        }
      ?> 
    </td>
  </tr>
  <tr>
    <td valign="top">
      <b>Present Illness :</b> <?php echo $pi->pi; ?>      
    </td>
  </tr>
  <tr>
    <td rowspan="2" valign="top">
      <b>Past History : </b><?php echo $ph->ph; ?>    
    </td>
    <td colspan="4" valign="top">
      <b>Laboratory :</b> 
    <?php
      foreach($lab as $r){
        if($r->labcode == '222'){
          $creat = LCr::find()->where(['last_ln' => $r->ln])->one();
        }
        if($r->labcode == '001'){
          $cbc = Lcbc::find()->where(['ln' => $r->ln])->one();
        }
        if($r->labcode == '031'){
          $elyte = Lelyte::find()->where(['ln' => $r->ln])->one();
        }
        if($r->labcode == '306'){
          $dtx = Ldtx::find()->where(['ln' => $r->ln])->one();
        }

        $l = Lab::findOne($r->labcode);
        echo $l->labname.',';
      }
    ?>
    </td>
  </tr>
  <tr>
  <td colspan=2 valign="top"><b>ผลการชันสูตรที่สำคัญ</b>
      <br>
      <?php 
      if($labresult){
        echo 'Creatinine: '.$labresult->labresult.' mg/dL '.'<br>&emsp;';
        echo 'eGFR(CKD-EPI): '.egfr($cr->age,$labresult->labresult,$cr->sex).' mL/min/1.73^2 <br>&emsp;' ;
      }  
      if($dtx){
        echo 'DTX: '.$dtx->dtx; 
      }
      ?> 
    </td>
    <td valign="top" style="background-image:url(../web/imgs/cbc.jpg);background-repeat:no-repeat;background-size:160px 100px;   width: 160px; height: 100px;">
      <?php if($cbc){echo 'CBC : '?><br><br>&emsp;<?php echo $cbc->hb;?>&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; <?php echo $cbc->pmn; ?>
      <br> &emsp; &emsp; &emsp; &emsp;&emsp;<?php echo $cbc->wbc; ?> <br>&emsp;
      <br> &emsp; &emsp; &emsp; &emsp; <?php echo $cbc->pltc; ?> <br>&emsp;       
      <br> &emsp; <?php echo $cbc->hct;?>&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; <?php echo $cbc->lym;} ?>
    </td>
    <td valign="top" style="background-image:url(../web/imgs/elyte.jpg);background-repeat:no-repeat;background-size:160px 100px;   width: 160px; height: 100px;">
      <?php if($elyte){echo 'Elyte : '?> <br><br>&emsp;&emsp; &emsp;
      <?php echo $elyte->na;?>&emsp; &emsp; <?php echo $elyte->k; ?>
      <br> &emsp;<br> &emsp;<br>&emsp;&emsp; &emsp; 
      <?php echo $elyte->cl;?>&emsp; &emsp; <?php echo $elyte->co2; }?></td>
  </tr>
  <tr>
    <td rowspan="2" valign="top">
      <b>Physical Exam :</b> <br><br>
      <img width=200 align="center" src="../web/imgs/imgpe.jpg">      
      <p><?php echo $pe->pe ?></p>    
    </td>
    <td colspan="4" valign="top">
      </b>X-Ray : </b> 
      <?php
        foreach($xry as $r){
          $l = Xray::findOne($r->xrycode);
          echo $l->xryname.',';
        }
      ?>    
    </td>
  </tr>
  <tr>
    <td colspan="4" valign="top">
    <table>
    <tr>
      <td width=40%><b>Rx : ชื่อยา</b></td>
      <td width=40%><b>วิธีใช้</b></td>
      <td width=10%><b>จำนวน</b></td>
      <td width=10%><b>ED/NED</b></td>
    </tr>
    <?php
      foreach($rx as $r){
        $unit = Meditem::findOne($r->meditem);
        if($unit->ed ==1){
          $ed = 'ED';
        } else {
          $ed = 'NED';
        }
        $use = Medusage::find()->where(['dosecode' => $r->medusage])->one();
        echo '<tr><td>'.$r->nameprscdt.'</td><td>'. $use->doseprn1.' '.$use->doseprn2.'</td><td>'.$r->qty.' '.$unit->pres_unt.'</td><td>'.$ed.'</td></tr>';
      }
    ?>
    </table>
    </td>
  </tr>
  <tr>
    <td>
      <b>Consult : /<b><?php echo $consult->detail; ?>
    </td>
    <td colspan="4">
      <b>นัด : </b>    
    <?php
      foreach($appoint as $r){
        $c = Clinic::find()->where('substr(cln,1,1) = :cln',[':cln' => substr($r->cln,0,1)])->one();
        $fd = new DateTime($r->fudate);
        echo $c->namecln.':'.$r->dscrptn.' วันที่ '.date_format($fd,'d/m/Y').'<br>';
      }
    ?>
    </td>  
  </tr>    
  <tr>
    <td valign="top">
      <b>Procedure : </b> 
    <?php
      foreach($proc as $pr){
        echo $pr->icd9cm.'=>'.$pr->icd9name.',';
      }
    ?>
    </td>
    <td rowspan="3" colspan="4" valign="top">
      <b>คำแนะนำ : </b><?php echo $advice->advice; ?>
    </td>
  </tr>
  <tr>
    <td valign="top">
      <b>แพทย์ผู้รักษา </b> 
    <?php 
      if(strlen($model->dct) == 5 ){
        $doctor = Dct::find()->where(['lcno' => $model->dct])->one();
      } else {
        $doctor = Dct::find()->where(['dct' => substr($model->dct,2,2)])->one();
      }
      echo $doctor->fname.' '. $doctor->lname .'<br> เลขที่ใบประกอบวิชาชีพ :'.$doctor->lcno;
    ?>
    </td>
  </tr>
  <tr>
    <td valign="top">
      <b>สถานะจำหน่าย : </b> 
    <?php 
      if($model->ovstost == 1){
        echo "กลับบ้าน";
      } else if($model->ovstost == 2){
        echo "เสียชีวิต";
      } else if($model->ovstost == 4){
        echo "รับรักษาในโรงพยาบาล AN:" . $model->an;
      } else if($model->ovstost == 3){
        $h = Hospcode::find()->where(['off_id' =>$refer->rfrlct])->one();
      echo "ส่งต่อ :".$h->namehosp;
      }
    ?> 
    </td>
  </tr>
</table>
</p>
