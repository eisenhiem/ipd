<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Meditem;
use app\models\Lab;
use app\models\Xray;
use app\models\Clinic;
use app\models\Hospcode;
use app\models\Dct;
use app\models\Lcbc;
use app\models\Lelyte;
use app\models\LCr;
use app\models\Ldtx;
use app\models\Medusage;
use app\models\Labresult;

$birthdate = new DateTime($ptinfo->birth);
$visit = new DateTime($model->vstdttm);
$visit_date = date_format($visit,'d/m/Y');
$visit_time = date_format($visit,'H:i');

function age($bdate,$vdate){
  $difference = $bdate->diff($vdate); 
  
  $age = $difference->format('%y');  
  return $age;
} 

function egfr($age,$creat,$sex){
  if ($creat){
    if($sex == '1'){
      if($creat > 0.9) {
        $egfr = 141  * pow(0.993,$age) / pow(($creat/0.9),1.209);
      } else {
        $egfr = 141  * pow(0.993,$age) / pow(($creat/0.9),0.411);
      }
    } else {
      if($creat > 0.9) {
        $egfr = 144  * pow(0.993,$age) / pow(($creat/0.7),1.209);
      } else {
        $egfr = 144  * pow(0.993,$age) / pow(($creat/0.7),0.329);
      }
    }  
  } else {
    $egfr = 'NA';
  }
  return round($egfr,2);
}

?>
<table width=100%>
  <tr>
    <td colspan="2" align="center"><b>บันทึกการรักษาผู้ป่วย NCD โรงพยาบาลเหล่าเสือโก้ก อำเภอเหล่าเสือโก้ก จังหวัดอุบลราชธานี</b></td>
  </tr>
  <tr>
    <td width="50%">
    <b>ชื่อ :</b> <?php echo $ptinfo->prename.$ptinfo->ptname; ?> <b>เพศ :</b><?php echo $ptinfo->sex;?> 
    <b>อายุ :</b> <?php echo age($birthdate,$visit); ?> ปี <b>HN :</b><?php echo $model->hn; ?><br>
    <b>เลขบัตรประชาชน :</b><?php echo $ptinfo->cid;?> <b>รับบริการวันที่</b> <?php echo $visit_date; ?>
    </td>
    <td rowspan="11" style="vertical-align:top;height:100%">
    <table width="100%" style="border-collapse: collapse">
        <tr>
          <td width="20">No.</td>
          <td width=40% align="center"><b>ชื่อยา</b></td>
          <td width=40% align="center"><b>วิธีใช้</b></td>
          <td width="20" align="center"><b>#</b></td>
        </tr>
    <?php
      $i=0;
      foreach($rx as $r){
        $i++;
        $use = Medusage::find()->where(['dosecode' => $r->medusage])->one();
        echo '<tr><td align="center" style="border-bottom:1pt solid black">'.$i.'</td><td style="font-size:0.8em;border-bottom:1pt solid black"> '.$r->nameprscdt.'</td><td style="font-size:0.8em;border-bottom:1pt solid black">'. $use->doseprn1.' '.$use->doseprn2.'</td><td align="right" style="border-bottom:1pt solid black">'.$r->qty.'</td></tr>';
      }
    ?>
      </table>
    </td>
  </tr>
  <tr>
    <td>
    <b>V/S</b>
  BT: <?php echo $model->tt; ?> ºC, BP: <?php echo $model->sbp.'/'.$model->dbp; ?> mmHg, PR: <?php echo $model->pr?> /m, RR: <?php echo $model->rr?> /m,
  BW: <?php echo $model->bw; ?> kg, ส่วนสูง:<?php echo $model->height?> cm, BMI: <?php if($model->height <> 0) { echo round(($model->bw)/(($model->height/100)*($model->height/100)),2);} ?><br> 
    </td>
  </tr>
  <tr>
    <td><b>CC:</b> <?= $cc->symptom; ?></td>
  </tr>
  <tr>
    <td><b>PI:</b> <?= $pi->pi; ?></td>
  </tr>
  <tr>
      <td><b>PE:</b> <?= $pe->pe ?></td>
  </tr>
  <tr>
    <td><b>ประวัติแพ้ยา:</b> <?= $allergy->drug ?></td>
  </tr>
  <tr>
    <td>
    <?php 
      if($lab){echo '<b>Lab :</b>';}
      foreach($lab as $r){
        $l = Lab::findOne($r->labcode);
        $result = Labresult::find()->where(['ln' => $r->ln])->all();
        foreach($result as $s){
          echo $s->getLabelName().'=>'.$s->labresult.', ';
          if($s->lab_code_local == 'CREAT'){
            echo 'eGFR(CKD-EPI): '.egfr($cr->age,$s->labresult,$cr->sex).', ' ;
          }  
        }
      }
    ?>    
    </td>
  </tr>
  <tr>
    <td><b>Diagnosis :</b> 
      <?php 
        foreach($diag as $dx){
          echo $dx->icd10.", ";
        }
      ?> 
    </td>
  </tr>
  <tr>
    <td><b>แพทย์</b> 
    <?php 
      if(strlen($model->dct) == 5 ){
        $doctor = Dct::find()->where(['lcno' => $model->dct])->one();
      } else {
        $doctor = Dct::find()->where(['dct' => substr($model->dct,2,2)])->one();
      }
      echo $doctor->fname.' '. $doctor->lname;
    ?>
    </td>
  </tr>
  <tr>  
    <td>
    <h1 stlye="font-size:4em">
    <?php
      foreach($appoint as $r){
        $c = Clinic::find()->where("substr(cln,1,1) = :cln and substr(cln,1,1) not in ('7','4','2','8')",[':cln' => substr($r->cln,0,1)])->one();
        $fd = new DateTime($r->fudate);
        if(substr($r->cln,0,1)=='S'){
          $clnname = 'คลินิกเฉพาะโรค';
        } else {
          $clnname = $c->namecln;
        }
        if($c){
          echo 'นัดวันที่ <b>'.date_format($fd,'d/m/Y').'</b></h1> '.$clnname.':'.$r->dscrptn;
        }
      }
      echo '<br>';
      foreach($appoint as $r){
        $c = Clinic::find()->where("substr(cln,1,1) = :cln and substr(cln,1,1) in ('7','4','2','8')",[':cln' => substr($r->cln,0,1)])->one();
        if($c){
          echo $r->dscrptn.' , ';
        }
      }
    ?>
    </td>
  </tr>
  <tr>
    <td><b>คำแนะนำ : </b><?php echo $advice->advice; ?></td>
  </tr>
</table>